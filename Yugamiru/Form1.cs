﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;
using System.Net;
using QRCoder;
using System.IO;
using static Yugamiru.QRCode;
using System.Resources;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Yugamiru
{
    public partial class IDD_BALANCELABO_DIALOG : Form
    {
        //Bitmap m_btnMeasurement;

        // m_btnAnalysis;
        //CBitmapButton m_btnClose;
        //CBitmapButton m_btnSetting;
        public JointEditDoc GetDocument = new JointEditDoc();
        ImageClipWnd ICW = new ImageClipWnd();
     public byte[] pbytes = null;

        string print;
     
        Bitmap m_hBitmapQRCode;
        public byte[] m_pbyteQRCodeBitmapBits;
        int m_iQRCodeBitmapWidth;
        int m_iQRCodeBitmapHeight;
        PictureBox picturebox1 = new PictureBox();
        Image imgQRcode;
        Bitmap bmBack1;


        public IDD_BALANCELABO_DIALOG()
        {
            InitializeComponent();
            this.qRCODEToolStripMenuItem.Enabled = true;
            this.pORTSETTINGToolStripMenuItem.Enabled = true;
            GetDocument.OnNewDocument();
            //this.Size = new Size(1366, 728);
            /*this.Size = new Size(1330, 650);
            this.Location = new Point(0, 0);*/
            //this.WindowState = FormWindowState.Maximized;

            //this.MinimumSize = new Size(this.Width, this.Height);

            // no larger than screen size
            this.MaximumSize = new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);// (int)SystemParameter.PrimaryScreenHeight);
            this.Size = new Size(this.MaximumSize.Width, this.MaximumSize.Height);

            

            /* size of the array m_pbyteQRCodeBitmapBits is given as 100000 for time being*/
           //m_pbyteQRCodeBitmapBits = new byte[100000];
           // m_iQRCodeBitmapWidth = 0;
           // m_iQRCodeBitmapHeight = 0;
           // if (m_hBitmapQRCode != null)
             //   m_hBitmapQRCode.Dispose();

            //start here

            //if (m_hBitmapQRCode == null)
           // {

                //bmBack1 = Yugamiru.Properties.Resources.Mainpic;
                /*this.Controls.Add(picturebox1);

                picturebox1.Size = new Size(bmBack1.Size.Width, bmBack1.Size.Height);
                //picturebox1.SizeMode = PictureBoxSizeMode.CenterImage;
                //picturebox1.BackgroundImageLayout = ImageLayout.Center;
                // picturebox1.Anchor = AnchorStyles.Left;

                picturebox1.Left = (this.ClientSize.Width - picturebox1.Width) / 2;
                //picturebox1.Top = (this.ClientSize.Height - picturebox1.Height) / 2;
                picturebox1.Top = 25;
                picturebox1.Image = bmBack1;*/

                menuStrip1.BackColor = Color.White;


                // Set the border style to a three-dimensional border.
                //picturebox1.BorderStyle = BorderStyle.FixedSingle;
                

                this.AutoScroll = true;
                //this.BackgroundImageLayout = ImageLayout.Center;

                //m_iQRCodeBitmapWidth = bmBack1.Size.Width;
                //m_iQRCodeBitmapHeight = bmBack1.Size.Height;


                string host; // ƒ|[ƒg”Ô†‚ÍÅ‘å5Œ…+ƒRƒƒ“+‹ó”’•¶Žš.
                string hostName = Dns.GetHostName(); // Retrive the Name of HOST
                // Get the IP
                string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();

                // theApp.GetMyIPAddress(host, sizeof(host));





                int szPort;
                //wsprintf(szPort, ":%d", theApp.GetServerPort());
                szPort = Constants.DEFAULT_PORT_NUMBER;
                //strcat(host, szPort.ToString);
                host = myIP +":"+ szPort.ToString();
                byte[] host_toBytes = Encoding.ASCII.GetBytes(host);

                /*   
                 *  QRCode generation using QRCoder in C#*/
                   QRCodeGenerator qrGenerator = new QRCodeGenerator();
                   QRCodeGenerator.QRCode qrCode = qrGenerator.CreateQrCode(host, QRCodeGenerator.ECCLevel.Q);
                 //System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
                
                   
                   

                   Bitmap QRCodeBitmap1 = qrCode.GetGraphic(20);
                GetDocument.SetQRCodeImage(QRCodeBitmap1);
                 /*  using (QRCodeBitmap1 = qrCode.GetGraphic(20))
                   {
                       using (MemoryStream ms = new MemoryStream())
                       {
                           QRCodeBitmap1.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                           byte[] byteImage = ms.ToArray();
                           //imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);

                           imgQRcode = (Bitmap)((new ImageConverter()).ConvertFrom(byteImage));
                       }
                   }*/
               // imgQRcode = QRCodeBitmap1;

             //CQRCodeBitmap QRCodeBitmap = new CQRCodeBitmap();
               // QRCode QRCode = new QRCode();
                //int iBestMask = QRCode.CreateQRCodeBitmap(QRCodeBitmap, Constants.ENCODEMODE_ALPHANUMERIC, 0, Constants.ECCTYPE_H, host_toBytes);
                //		int iBestMask = CreateQRCodeBitmap( QRCodeBitmap, ENCODEMODE_BINARY, 0, ECCTYPE_Q, "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz" );
                //		int iBestMask = CreateQRCodeBitmap( QRCodeBitmap, ENCODEMODE_ALPHANUMERIC, 0, ECCTYPE_H, "ABCDE123" );

                //int iWidthStep = ((m_iQRCodeBitmapWidth + 7) / 8 + 3) / 4 * 4;
               // int i = 0;
               // for (i = 0; i < m_iQRCodeBitmapHeight; i++)
               // {
                   // int j = 0;
                  //  for (j = 0; j < iWidthStep; j++)
                    //{
                      //  m_pbyteQRCodeBitmapBits[i * iWidthStep + j] = 0;
                 //   }
                //}
                //for (i = 0; i < QRCodeBitmap.GetSize(); i++)
                //{
                    //int j = 0;
                   // for (j = 0; j < QRCodeBitmap.GetSize(); j++)
                    //{
                       // int k = 0;
                        //for (k = 0; k < 16; k++)
                       // {
                            //if ((QRCodeBitmap.Index(i, j) > 0) && (1 < iBestMask))
                            //{
                                //m_pbyteQRCodeBitmapBits[(m_iQRCodeBitmapHeight - 1 - (50 + 16 * i + k)) * iWidthStep + (282 / 8) + j * 2 + 0] = 0xFF;
                                //m_pbyteQRCodeBitmapBits[(m_iQRCodeBitmapHeight - 1 - (50 + 16 * i + k)) * iWidthStep + (282 / 8) + j * 2 + 1] = 0xFF;
                           // }
                       // }
                   // }
               // }

                panel2.Height = this.Size.Height ;
                panel2.Width = this.Size.Width ;
                //panel2.Location = new Point(0, 0);
                //panel2.Top = 30;

                IDD_BALANCELABO m_BALANCELABO = new IDD_BALANCELABO(GetDocument);

            this.Show();
          //  m_BALANCELABO.Show();
            m_BALANCELABO.Width = panel2.Width;
                m_BALANCELABO.Height = panel2.Height;
                //m_BALANCELABO.AutoScroll = true;
                

                m_BALANCELABO.TopLevel = false;
                
                panel2.BorderStyle = BorderStyle.Fixed3D;
                panel2.Controls.Add(m_BALANCELABO);
                
                panel2.Show();
            m_BALANCELABO.Show();


            // to capture the event and to call back the function M_BALANCELABO_closeForm - step 4

            //Edited By Suhana......GSP-313.....................//
            //Condition checking

            if (GlobalVar.isAutomatic)
            {
                m_BALANCELABO.IDC_MeasurementBtn_Click(null, null);
                IDD_MeasurementDlg imds = new IDD_MeasurementDlg(GetDocument);
                imds.Show();
                showRecords show = new showRecords();
                    M_BALANCELABO_closeForm(null, null);

            }
            //    //*********************************presently in hold due to GSP-318**********************
            //    // imds.IDC_NextBtn_Click(null,null);
            //    // M_MeasurementDlg_EventToStartNextScreen(null, null);
            //    //*********************************presently in hold**********************
            //}
            else
            {
                m_BALANCELABO.closeForm += M_BALANCELABO_closeForm;

            }
            ///////////////////////////////GSP-313////////////////////////
           // m_BALANCELABO.closeForm += M_BALANCELABO_closeForm;
            m_BALANCELABO.OpenSettingScreen += M_BALANCELABO_OpenSettingScreen;


        }

        private void M_BALANCELABO_OpenSettingScreen(object sender, EventArgs e)
        {
            this.qRCODEToolStripMenuItem.Enabled = false;
            this.pORTSETTINGToolStripMenuItem.Enabled = false;

            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();

            SettingView SettingView = new SettingView(GetDocument);

            SettingView.Width = panel2.Width;
            SettingView.Height = panel2.Height;
            SettingView.AutoScroll = true;

            SettingView.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(SettingView);
            panel2.Show();

            SettingView.Show();
            SettingView.EventToGoInitialScreen += SettingView_EventToGoInitialScreen;
        }

        private void SettingView_EventToGoInitialScreen(object sender, EventArgs e)
        {

            // panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();

            panel2.Controls.Clear();
            RefreshMenuStrip(true);
            IDD_BALANCELABO m_Balancelabo_Dialog = new IDD_BALANCELABO(GetDocument);
            m_Balancelabo_Dialog.Width = panel2.Width;
            m_Balancelabo_Dialog.Height = panel2.Height;
            m_Balancelabo_Dialog.AutoScroll = true;

            m_Balancelabo_Dialog.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_Balancelabo_Dialog);
            panel2.Show();

            m_Balancelabo_Dialog.Show();
            m_Balancelabo_Dialog.closeForm += M_BALANCELABO_closeForm;
            m_Balancelabo_Dialog.OpenSettingScreen += M_BALANCELABO_OpenSettingScreen;
        }

        public void M_BALANCELABO_closeForm(object sender, EventArgs e)
        {
            RefreshMenuStrip(false);
            // Close the IDD Form
            GetDocument.SetInputMode(Constants.INPUTMODE_NEW);
            GetDocument.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_INITIALIZE);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();
            IDD_MeasurementDlg m_MeasurementDlg = new IDD_MeasurementDlg(GetDocument);
            m_MeasurementDlg.Width = panel2.Width;
            m_MeasurementDlg.Height = panel2.Height;
            m_MeasurementDlg.AutoScroll = true;

            m_MeasurementDlg.TopLevel = false;
            panel2.BorderStyle = BorderStyle.FixedSingle;
            panel2.Controls.Add(m_MeasurementDlg);
            panel2.Show();

            /////////////////////////Edited by Suhana for GSP-318
            if (1==0)//GlobalVar.isAutomatic)
            {
                showRecords shw = new showRecords();
                m_MeasurementDlg.IDC_ID.Text = showRecords.Patient_ID;
                m_MeasurementDlg.IDC_Name.Text = showRecords.Patient_Name;
                // m_MeasurementDlg.IDC_BirthYear.Value = showRecords.
                m_MeasurementDlg.IDC_Height.Value = Convert.ToDecimal(showRecords.Height);
                if (showRecords.Patient_Gender == "M")
                {
                    m_MeasurementDlg.IDC_COMBO_GENDER.SelectedItem = "’j";
                }
                else
                    m_MeasurementDlg.IDC_COMBO_GENDER.SelectedItem = "—";
                m_MeasurementDlg.Show();
                string dateOFBirth = showRecords.Patient_DOB;               

                string year = dateOFBirth.Split('-')[0];
                string month= dateOFBirth.Split('-')[1];
                string months = month.Substring(1);
                string date = dateOFBirth.Split('-')[2];
                string dates = date.Substring(1);           
                
                m_MeasurementDlg.IDC_BirthYear.Value = Convert.ToDecimal(year);
                  m_MeasurementDlg.IDC_COMBO_MONTH.SelectedItem = months;
              //  m_MeasurementDlg.IDC_COMBO_MONTH.SelectedItem = "3";
                this.Refresh();

                

                m_MeasurementDlg.IDC_COMBO_DAY.SelectedItem = dates;
                M_MeasurementDlg_closeForm(null, null);
               M_MeasurementDlg_EventToStartNextScreen(null,null);

            }
            else  
            {

                //Sumit 0712 B1209 Adding for assigning name, dob etc from Database (showrecord)
                if (GlobalVar.isAuto_New0512)
                {
                    showRecords shw = new showRecords();
                    m_MeasurementDlg.IDC_ID.Text = showRecords.Patient_ID;
                    m_MeasurementDlg.IDC_Name.Text = showRecords.Patient_Name;
                    // m_MeasurementDlg.IDC_BirthYear.Value = showRecords.
                    m_MeasurementDlg.IDC_Height.Value = Convert.ToDecimal(showRecords.Height);
                    if (showRecords.Patient_Gender == "M")
                    {
                        m_MeasurementDlg.IDC_COMBO_GENDER.SelectedItem = "’j";
                    }
                    else
                        m_MeasurementDlg.IDC_COMBO_GENDER.SelectedItem = "—";
                    m_MeasurementDlg.Show();
                    string dateOFBirth = showRecords.Patient_DOB;

                    string year = dateOFBirth.Split('-')[0];
                    string month = dateOFBirth.Split('-')[1];
                    string months = month.Substring(1);
                    string date = dateOFBirth.Split('-')[2];
                    string dates = date.Substring(1);

                    m_MeasurementDlg.IDC_BirthYear.Value = Convert.ToDecimal(year);
                    m_MeasurementDlg.IDC_COMBO_MONTH.SelectedItem = months;
                    //  m_MeasurementDlg.IDC_COMBO_MONTH.SelectedItem = "3";
                    this.Refresh();
                    //return;
                }
                //Sumit 0712 B1209 END

                m_MeasurementDlg.Show();
                 m_MeasurementDlg.closeForm += M_MeasurementDlg_closeForm;
                 m_MeasurementDlg.EventToStartNextScreen += M_MeasurementDlg_EventToStartNextScreen;
            }

            ////////////////////////////////GSP-318 End//////
            // m_MeasurementDlg.closeForm += M_MeasurementDlg_closeForm;
            // m_MeasurementDlg.EventToStartNextScreen += M_MeasurementDlg_EventToStartNextScreen;


        }
     
        public void M_MeasurementDlg_EventToStartNextScreen(object sender, EventArgs e)
        {
            RefreshMenuStrip(false);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();
            IDD_MEASUREMENT_START_VIEW m_MEASUREMENT_START = new IDD_MEASUREMENT_START_VIEW(GetDocument);

            m_MEASUREMENT_START.Width = panel2.Width;
            m_MEASUREMENT_START.Height = panel2.Height;
            m_MEASUREMENT_START.AutoScroll = true;

            m_MEASUREMENT_START.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_MEASUREMENT_START);
            panel2.Show();
          //  m_MEASUREMENT_START.Show();
            ////********************************************************GSP-319****************************************///
            if (GlobalVar.isAutomatic)
            {
                /////For Time Being....
                m_MEASUREMENT_START.closeForm += M_BALANCELABO_closeForm;
                ////////
                // showRecords recs = new showRecords();
                m_MEASUREMENT_START.IDC_ID.Text = showRecords.Patient_ID;
                m_MEASUREMENT_START.IDC_Name.Text = showRecords.Patient_Name;
                m_MEASUREMENT_START.IDC_Gender.Text = showRecords.Patient_Gender;


                // DateTime date = new DateTime(dates);

                m_MEASUREMENT_START.IDC_DoB.Text = Convert.ToDateTime(showRecords.Patient_DOB).ToString("dd-MM-yyyy");
                m_MEASUREMENT_START.IDC_Height.Text = showRecords.Height;


                byte[] sideIMG = showRecords.sideimg_getBytes;
                File.WriteAllBytes(Application.StartupPath + @"\sidePICIMG.JPG", sideIMG);


                m_MEASUREMENT_START.ImagePrevWnd.Image = Image.FromFile(Application.StartupPath + @"\sidePICIMG.JPG");
                /////For Time Being....
                m_MEASUREMENT_START.IDC_ShootBtn_Click(null, null);


                //////
                m_MEASUREMENT_START.ImageClipWnd.Image = Image.FromFile(Application.StartupPath + @"\sidepic.JPG");

                m_MEASUREMENT_START.ImagePrevWnd.SizeMode = PictureBoxSizeMode.StretchImage;
                m_MEASUREMENT_START.ImageClipWnd.SizeMode = PictureBoxSizeMode.StretchImage;
                m_MEASUREMENT_START.IDC_NextBtn_Click(null, null);
                 //m_MEASUREMENT_START.Show();



                // M_BALANCELABO_closeForm(null,null);
                  m_MEASUREMENT_START.Show();

                //byte[] standing_IMG = showRecords.standing_getBytes;
                //JointEditDoc joint = new JointEditDoc();
                //joint.m_FrontStandingImageBytes = standing_IMG;

                JointEditDoc.m_iMeasurementStartViewMode = 1;

                byte[] standing_IMG = showRecords.standing_getBytes;
                JointEditDoc joint = new JointEditDoc();
                joint.m_FrontStandingImageBytes = standing_IMG;
                JointEditDoc.m_iMeasurementStartViewMode = 1;
                GlobalVar.imgByte = showRecords.standing_getBytes;
                m_MEASUREMENT_START.IDC_NextBtn_Click(null, null);





             


                File.WriteAllBytes(Application.StartupPath + @"\StandingIMG.JPG", joint.m_FrontStandingImageBytes);
                m_MEASUREMENT_START.ImagePrevWnd.Image = Image.FromFile(Application.StartupPath + @"\StandingIMG.JPG");
                m_MEASUREMENT_START.IDC_ShootBtn_Click(null, null);
                m_MEASUREMENT_START.ImageClipWnd.Image = Image.FromFile(Application.StartupPath + @"\StandingIMG.JPG");
                m_MEASUREMENT_START.ImagePrevWnd.SizeMode = PictureBoxSizeMode.StretchImage;
                m_MEASUREMENT_START.ImageClipWnd.SizeMode = PictureBoxSizeMode.StretchImage;
                // m_MEASUREMENT_START.IDC_NextBtn_Click(null, null);
                m_MEASUREMENT_START.Show();


                this.Refresh();
                JointEditDoc.m_iMeasurementStartViewMode = 2;
                byte[] crouched_IMG = showRecords.crouchedimg_getBytes;
         
                joint.m_FrontKneedownImageBytes = crouched_IMG;
                JointEditDoc.m_iMeasurementStartViewMode = 2;
                GlobalVar.imgByte = crouched_IMG;
                m_MEASUREMENT_START.IDC_NextBtn_Click(null,null);

                File.WriteAllBytes(Application.StartupPath + @"\CrouchedIMG.JPG", joint.m_FrontKneedownImageBytes);
                m_MEASUREMENT_START.ImagePrevWnd.Image = Image.FromFile(Application.StartupPath + @"\CrouchedIMG.JPG");
                m_MEASUREMENT_START.IDC_ShootBtn_Click(null, null);

                //if (m_MEASUREMENT_START.ImageClipWnd != null)
                //{
                //    m_MEASUREMENT_START.ImageClipWnd.Image.Dispose();
                //}

                //using (Image myImage = Image.FromFile(Application.StartupPath + @"\CrouchedIMG.JPG"))
                //{
                //    m_MEASUREMENT_START.ImageClipWnd.Image = (Image)myImage.Clone();
                //    m_MEASUREMENT_START.ImageClipWnd.Update();
                //}
                   
                m_MEASUREMENT_START.ImageClipWnd.Image = Image.FromFile(Application.StartupPath + @"\CrouchedIMG.JPG");
                m_MEASUREMENT_START.ImagePrevWnd.SizeMode = PictureBoxSizeMode.StretchImage;
                m_MEASUREMENT_START.ImageClipWnd.SizeMode = PictureBoxSizeMode.StretchImage;
                // m_MEASUREMENT_START.IDC_NextBtn_Click(null, null);
                m_MEASUREMENT_START.Show();

                JointEditDoc.m_iMeasurementStartViewMode = 3;
                m_MEASUREMENT_START.IDC_NextBtn_Click(null, null);
                M_MeasurementDlg_Closeformtostartnextscreen(null, null);





                ///Time Being......
                //  M_MeasurementDlg_Closeformtostartnextscreen(null,null);


                // m_MEASUREMENT_START.EventfromCrouchedViewtoResultView += M_SideJointEditView_EventToChangeResultView;




            }
            else
            {
                

                    m_MEASUREMENT_START.Show();



                m_MEASUREMENT_START.closeForm += M_BALANCELABO_closeForm;
                m_MEASUREMENT_START.EventToStartNextScreen += M_MeasurementDlg_Closeformtostartnextscreen;
                m_MEASUREMENT_START.EventfromCrouchedViewtoResultView += M_SideJointEditView_EventToChangeResultView;
                //sumit 061217a
                if (GlobalVar.isAuto_New0512)
                {
                    //m_MEASUREMENT_START.IDC_NextBtn_Click(null, null);
                    GlobalVar.Step_counter_OpenImg = 1;
                    m_MEASUREMENT_START.IDC_SearchBtn_Click(null, null);
                    m_MEASUREMENT_START.IDC_ShootBtn_Click(null, null);
                    m_MEASUREMENT_START.IDC_NextBtn_Click(null, null);

                    GlobalVar.Step_counter_OpenImg = 2;
                    m_MEASUREMENT_START.IDC_SearchBtn_Click(null, null);
                    m_MEASUREMENT_START.IDC_ShootBtn_Click(null, null);
                    m_MEASUREMENT_START.IDC_NextBtn_Click(null, null);

                    GlobalVar.Step_counter_OpenImg = 3;
                    m_MEASUREMENT_START.IDC_SearchBtn_Click(null, null);
                    m_MEASUREMENT_START.IDC_ShootBtn_Click(null, null);
                    m_MEASUREMENT_START.IDC_NextBtn_Click(null, null);
                }
                //sumit 061217a END

                //Sumit 0512-18-1
                if (GlobalVar.isAuto_New0512)
                {
                    //IDC_MeasurementBtn_Click(null, null);
                //    M_BALANCELABO_closeForm(null, null);
                //    M_MeasurementDlg_Closeformtostartnextscreen(null, null);
                //    M_SideJointEditView_EventToChangeResultView(null, null);
                }
            //Sumit 0513-18-1 END

            }
            #region cMT

            ////********************************************************END GSP-319****************************************///


            /////______________________________This task is closed presently due to GSP 319 ________________//

            //  /////Edited By Suhana............GSP-313.........................

            //if(GlobalVar.isAutomatic)
            //   { 
            //  m_MEASUREMENT_START.closeForm += M_BALANCELABO_closeForm;//M_MeasurementDlg_closeForm;

            //  m_MEASUREMENT_START.IDC_ShootBtn_Click(null, null);
            //  m_MEASUREMENT_START.IDC_NextBtn_Click(null, null);

            //  m_MEASUREMENT_START.EventToStartNextScreen += M_MeasurementDlg_Closeformtostartnextscreen;
            //  m_MEASUREMENT_START.IDC_ShootBtn_Click(null, null);
            //  m_MEASUREMENT_START.IDC_NextBtn_Click(null, null);

            //  m_MEASUREMENT_START.EventToStartNextScreen += M_MeasurementDlg_Closeformtostartnextscreen;
            //  m_MEASUREMENT_START.IDC_ShootBtn_Click(null, null);
            //  m_MEASUREMENT_START.IDC_NextBtn_Click(null, null);

            //  m_MEASUREMENT_START.EventfromCrouchedViewtoResultView += M_SideJointEditView_EventToChangeResultView;
            //  }

            //  else
            //  {
            //      m_MEASUREMENT_START.closeForm += M_BALANCELABO_closeForm;
            //      m_MEASUREMENT_START.EventToStartNextScreen += M_MeasurementDlg_Closeformtostartnextscreen;
            //      m_MEASUREMENT_START.EventfromCrouchedViewtoResultView += M_SideJointEditView_EventToChangeResultView;

            //  }
            ///////////////////////

            //___________________________________________________________________________________//
            #endregion
        }

       

        private void M_MeasurementDlg_closeForm(object sender, EventArgs e)
        {

            //Sumit Reset Global Automatic back to normal
            GlobalVar.isAuto_New0512 = false;
            //Sumit AutomaticEND
            this.qRCODEToolStripMenuItem.Enabled = true;
            this.pORTSETTINGToolStripMenuItem.Enabled = true;
            GetDocument.SetInputMode(Constants.INPUTMODE_NEW);
            //GetDocument.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_INITIALIZE);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();
            IDD_BALANCELABO m_BalanceLabo = new IDD_BALANCELABO(GetDocument);
            m_BalanceLabo.Width = panel2.Width;
            m_BalanceLabo.Height = panel2.Height;
            m_BalanceLabo.AutoScroll = true;

            m_BalanceLabo.TopLevel = false;
            panel2.BorderStyle = BorderStyle.FixedSingle;
            panel2.Controls.Add(m_BalanceLabo);
            panel2.Show();


            m_BalanceLabo.Show();
            m_BalanceLabo.closeForm += M_BALANCELABO_closeForm;
            m_BalanceLabo.OpenSettingScreen += M_BALANCELABO_OpenSettingScreen;

        }

       

        public void M_MeasurementDlg_Closeformtostartnextscreen(object sender, EventArgs e)
        {
            RefreshMenuStrip(false);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();
            //IDD_MEASUREMENT_START_VIEW m_MEASUREMENT_START = new IDD_MEASUREMENT_START_VIEW(GetDocument);

            JointEditView m_JointEditView = new JointEditView(GetDocument);

            m_JointEditView.Width = panel2.Width;
            m_JointEditView.Height = panel2.Height;
            m_JointEditView.AutoScroll = true;

            m_JointEditView.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_JointEditView);
            panel2.Show();

            m_JointEditView.Show();
            //MessageBox.Show("jsdsjjds");
            //if (GlobalVar.isAutomatic)
            //{
            //    m_JointEditView.Show();
            //    M_JointEditView_closeBeltAnkleForm(null,null);
            //    M_JointEditView_EventToStartSideJointEditView(null, null);
            //}
            //else
            //{ 
            //    m_JointEditView.closeBeltAnkleForm += M_JointEditView_closeBeltAnkleForm;
            //m_JointEditView.EventToStartSideJointEditView += M_JointEditView_EventToStartSideJointEditView;
            //}
            ////Edited By Suhana....GSP-313.................
            //if (GlobalVar.isAutomatic)
            //{ 
            //m_JointEditView.IDC_OkBtn_Click(null, null);

            //m_JointEditView.closeBeltAnkleForm += M_JointEditView_closeBeltAnkleForm;
            //m_JointEditView.EventToStartSideJointEditView += M_JointEditView_EventToStartSideJointEditView;
            //m_JointEditView.IDC_OkBtn_Click(null, null);
            //m_JointEditView.EventToStartSideJointEditView += M_JointEditView_EventToStartSideJointEditView;

            //}

            //else
            //{ 
            m_JointEditView.closeBeltAnkleForm += M_JointEditView_closeBeltAnkleForm;
            m_JointEditView.EventToStartSideJointEditView += M_JointEditView_EventToStartSideJointEditView;

            //Sumit 061217C START
            M_JointEditView_EventToStartSideJointEditView(null, null);
            //Sumit 061217C END

            //}
            //////////////////////////////GSP-313/////////////////////////
        }

        public void M_JointEditView_EventToStartSideJointEditView(object sender, EventArgs e)
        {
            RefreshMenuStrip(false);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();

            SideJointEditView m_SideJointEditView = new SideJointEditView(GetDocument);

            m_SideJointEditView.Width = panel2.Width;
            m_SideJointEditView.Height = panel2.Height;
            m_SideJointEditView.AutoScroll = true;

            m_SideJointEditView.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_SideJointEditView);
            panel2.Show();
            GetDocument.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_SIDE);
            m_SideJointEditView.Show();
            ///Edited By Suhana.............GSP-313....................
            if (GlobalVar.isAutomatic)
            {
                m_SideJointEditView.EventToChangeResultView += M_SideJointEditView_EventToChangeResultView;
                m_SideJointEditView.IDC_OkBtn_Click(null, null);

            }
            else
            {
                m_SideJointEditView.EventToChangeResultView += M_SideJointEditView_EventToChangeResultView;
               
            }
            m_SideJointEditView.GoBackToJointEditView += M_SideJointEditView_GoBackToJointEditView;
            ///////////////////////////////////GSP-313//////////////////////////////

            //Sumit Chage For Side To Report 0612171632

            if(GlobalVar.isAuto_New0512)
            {
                m_SideJointEditView.IDC_OkBtn_Click(null, null);
            }

            //Sumit Chage For Side To Report 0612171632 END

        }

        private void M_SideJointEditView_GoBackToJointEditView(object sender, EventArgs e)
        {
            RefreshMenuStrip(false);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();

            JointEditView m_JointEditView = new JointEditView(GetDocument);

            m_JointEditView.Width = panel2.Width;
            m_JointEditView.Height = panel2.Height;
            m_JointEditView.AutoScroll = true;

            m_JointEditView.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_JointEditView);
            panel2.Show();

            m_JointEditView.Show();
            m_JointEditView.closeBeltAnkleForm += M_JointEditView_closeBeltAnkleForm;
            m_JointEditView.EventToStartSideJointEditView += M_JointEditView_EventToStartSideJointEditView;
        }

      
        private void M_SideJointEditView_EventToChangeResultView(object sender, EventArgs e)
        {
            RefreshMenuStrip(false);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();

            ResultView m_ResultView = new ResultView(GetDocument);

            m_ResultView.Width = panel2.Width;
            m_ResultView.Height = panel2.Height;
            m_ResultView.AutoScroll = true;

            m_ResultView.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_ResultView);
            panel2.Show();
            GetDocument.SetFinalScreenMode(Constants.FINAL_SCREEN_MODE_TRUE);
            m_ResultView.Show();
            
            m_ResultView.EventToEditID += M_ResultView_EventToEditID;
            m_ResultView.EventToChange += M_MeasurementDlg_EventToStartNextScreen;
            m_ResultView.EventToCheckPosition += M_MeasurementDlg_Closeformtostartnextscreen;
            m_ResultView.EventToInitialScreen += M_MeasurementDlg_closeForm;
            m_ResultView.EventToRestart += M_ResultView_EventToEditID;
            
        }

        private void M_ResultView_EventToInitialScreen(object sender, EventArgs e)
        {
            /*this.qRCODEToolStripMenuItem.Enabled = true;
            this.pORTSETTINGToolStripMenuItem.Enabled = true;*/
            RefreshMenuStrip(true);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();

            IDD_BALANCELABO m_BalanceLabo = new IDD_BALANCELABO(GetDocument);

            m_BalanceLabo.Width = panel2.Width;
            m_BalanceLabo.Height = panel2.Height;
            m_BalanceLabo.AutoScroll = true;

            m_BalanceLabo.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_BalanceLabo);
            panel2.Show();

            m_BalanceLabo.Show();
            m_BalanceLabo.closeForm += M_BALANCELABO_closeForm;
            m_BalanceLabo.OpenSettingScreen += M_BALANCELABO_OpenSettingScreen;


        }

        private void M_ResultView_EventToEditID(object sender, EventArgs e)
        {
            //Sumit to reset the AUTOMATIC variable back to normal
            GlobalVar.isAuto_New0512 = false;

            //Sumit END


            RefreshMenuStrip(false);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();

            IDD_MeasurementDlg m_MeasurementView = new IDD_MeasurementDlg(GetDocument);

            m_MeasurementView.Width = panel2.Width;
            m_MeasurementView.Height = panel2.Height;
            m_MeasurementView.AutoScroll = true;

            m_MeasurementView.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_MeasurementView);
            panel2.Show();

            m_MeasurementView.Show();

            m_MeasurementView.EventfromMeasurementViewtoResultView += M_SideJointEditView_EventToChangeResultView;
            m_MeasurementView.closeForm += M_BALANCELABO_closeForm;//M_MeasurementDlg_closeForm;
            m_MeasurementView.EventToStartNextScreen += M_MeasurementDlg_EventToStartNextScreen;
        }

     

        private void M_JointEditView_closeBeltAnkleForm(object sender, EventArgs e)
        {
            DisposeForms();
            //panel2.Controls.RemoveAt(0);
            //GetDocument.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN);
            IDD_MEASUREMENT_START_VIEW m_MEASUREMENT_START = new IDD_MEASUREMENT_START_VIEW(GetDocument);
            m_MEASUREMENT_START.Width = panel2.Width;
            m_MEASUREMENT_START.Height = panel2.Height;
            m_MEASUREMENT_START.AutoScroll = true;

            m_MEASUREMENT_START.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_MEASUREMENT_START);
            panel2.Show();

            m_MEASUREMENT_START.Show();
            m_MEASUREMENT_START.closeForm += M_BALANCELABO_closeForm;//M_MeasurementDlg_closeForm;
            m_MEASUREMENT_START.EventToStartNextScreen += M_MeasurementDlg_Closeformtostartnextscreen;
        }

        public void IDC_MeasurementBtn_Click(object sender, EventArgs e)
        {
         /*  if (!(GetDocument()->ProRingCheck(FALSE)))
            {
                return;
            }*/
            GetDocument.SetInputMode(Constants.INPUTMODE_NEW);
            GetDocument.ClearMakerTouchFlag();
            GetDocument.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_INITIALIZE);
            GetDocument.InitBodyBalance();
            GetDocument.ClearStandingImage();
            GetDocument.ClearKneedownImage();
            GetDocument.ClearSideImage();
            GetDocument.SetDataMeasurementTime("");
            GetDocument.SetSaveFilePath("");
            GetDocument.ChangeToMeasurementView();

            
          /*  panel2.Height = this.Size.Height - 100 ;
            panel2.Width = this.Size.Width - 30 ;

            IDD_MeasurementDlg m_MeasurementDlg = new IDD_MeasurementDlg();
            m_MeasurementDlg.Width = panel2.Width;
            m_MeasurementDlg.Height = panel2.Height;
            m_MeasurementDlg.AutoScroll = true;
            
            m_MeasurementDlg.TopLevel = false;
            //panel1.Controls.RemoveAt(0);
            panel2.BorderStyle = BorderStyle.FixedSingle;
            panel2.Controls.Add(m_MeasurementDlg);
            panel2.Show();
            
            m_MeasurementDlg.Show();*/
            //picturebox1.Dispose();
            //IDC_MeasurementBtn.Dispose();
            //IDC_AnalysisBtn.Dispose();
            //IDC_CloseBtn.Dispose();
            //IDC_SETTING_BTN.Dispose();
            
        }

        private void IDD_BALANCELABO_DIALOG_SizeChanged(object sender, EventArgs e)
        {

            var t = panel2.Controls;

            foreach (var control in panel2.Controls)
            {
                var controlType = control.GetType();

                if (controlType.Name == "IDD_BALANCELABO")
                {
                    var tt = control as IDD_BALANCELABO;
                    tt.Width = panel2.Width;
                    tt.Height = panel2.Height;
                    tt.IDD_BALANCELABO_SizeChanged(sender, e);
                    tt.Refresh();
                }
                else if (controlType.Name == "IDD_MeasurementDlg")
                {
                    var tt = control as IDD_MeasurementDlg;
                    tt.Width = panel2.Width;
                    tt.Height = panel2.Height;
                    tt.IDD_MeasurementDlg_SizeChanged(sender, e);
                    //tt.Refresh();
                }
                else if (controlType.Name == "IDD_MEASUREMENT_START_VIEW")
                {
                    var tt = control as IDD_MEASUREMENT_START_VIEW;
                    tt.Width = panel2.Width;
                    tt.Height = panel2.Height;
                    tt.IDD_MEASUREMENT_START_VIEW_SizeChanged(sender, e);
                    //tt.Refresh();
                }
                else if (controlType.Name == "JointEditView")
                {
                    var tt = control as JointEditView;
                    tt.Width = panel2.Width;
                    tt.Height = panel2.Height;
                    tt.JointEditView_SizeChanged(sender, e);
                    //tt.Refresh();

                }
                else if (controlType.Name == "SideJointEditView")
                {
                   var tt = control as SideJointEditView;
                     tt.Width = panel2.Width;
                     tt.Height = panel2.Height;
                     tt.SideJointEditView_SizeChanged(sender, e);
                     //tt.Refresh();
                    //tt.Show();
                    
                }
                else if(controlType.Name == "ResultView")
                {
                    var tt = control as ResultView;
                    tt.Width = panel2.Width;
                    tt.Height = panel2.Height;
                    tt.ResultView_SizeChanged(sender, e);

                }
                else if(controlType.Name == "SettingView")
                {
                    var tt = control as SettingView;
                    tt.Width = panel2.Width;
                    tt.Height = panel2.Height;
                    tt.SettingView_SizeChanged(sender, e);

                }

            }
                //if(control.GetType)

            
             // MessageBox.Show("hi");

             // MessageBox.Show("hello");
             //  struct CLayoutInfo;
             //		{
             //			uint m_uiID; // �R���g���[���̂h�c.
             //			int m_iLeft;
             //			int m_iTop;
             //			int m_iWidth;
             //			int m_iHeight;
             //		};

            //C++ TO C# CONVERTER NOTE: This static local variable declaration (not allowed in C#) has been moved just prior to the method:
            /*     aSymbolAnbdIDPair[0] = new CSymbolAndIDPair { iID = BODYPOSITIONTYPEID_STANDING, pchSymbol = "Standing" };
                 CLayoutInfo[] s_aLayoutInfo = new CLayoutInfo[100];

                 s_aLayoutInfo[0] = new CLayoutInfo
                 {
                     m_uiID = IDC_MeasurementBtn,

                 }
                   {
                              { 1, -237-91, 214, 112, 42},
                              { IDC_AnalysisBtn, -56-91, 214, 112, 42}, 
                              { IDC_CloseBtn, 126-91, 214, 112, 42}, 
                              { IDC_SETTING_BTN, 308-91, 214, 112, 42}, 
                              { IDC_STATIC, 0, 0, 0, 0}
                    };

                       Rectangle rcSelf;*/

            /* this.IDC_MeasurementBtn.Size = new Size(112, 42);
             this.IDC_MeasurementBtn.Location = new Point(400, 614);

             this.IDC_AnalysisBtn.Size = new Size(112,42);
             this.IDC_AnalysisBtn.Location = new Point(550,614);

             this.IDC_CloseBtn.Size = new Size(112, 42);
             this.IDC_CloseBtn.Location = new Point(700, 614);

             this.IDC_SETTING_BTN.Size = new Size(112, 42);
             this.IDC_SETTING_BTN.Location = new Point(850, 614);*/

            //--to centre the picture box while resizing the form
            //picturebox1.Left = (this.ClientSize.Width - picturebox1.Width) / 2;
            //picturebox1.Top = (this.ClientSize.Height - picturebox1.Height) / 2;
            //picturebox1.Top = 25;
            //--end




            //GetClientRect(rcSelf);

            //C++ TO C# CONVERTER TODO TASK: Pointer arithmetic is detected on this variable, so pointers on this variable are left unchanged:
            /*   CLayoutInfo pLayoutInfo = OnSize_s_aLayoutInfo[0];
               while (pLayoutInfo.m_uiID != IDC_STATIC)
               {
                   Rectangle rcControlInWindowCoordinate;
               CWnd pWnd = GetDlgItem(pLayoutInfo.m_uiID);
                   if (pWnd != null)
                   {
                       pWnd.MoveWindow(pLayoutInfo.m_iLeft + rcSelf.Width() / 2, 
                       pLayoutInfo.m_iTop + rcSelf.Height() / 2, 
                       pLayoutInfo.m_iWidth, pLayoutInfo.m_iHeight, 1);
                   }
           pLayoutInfo++;
               }
               }*/

         /*   IDC_MeasurementBtn.Left = this.Width / 2 - IDC_MeasurementBtn.Left;
            IDC_AnalysisBtn.Left = IDC_MeasurementBtn.Left + IDC_AnalysisBtn.Width + 90;
            IDC_CloseBtn.Left = IDC_AnalysisBtn.Left + IDC_CloseBtn.Width + 90; 
            IDC_SETTING_BTN.Left = IDC_CloseBtn.Left + IDC_SETTING_BTN.Width + 90;*/


            /*  IDC_MeasurementBtn.Size = new Size(112, 42);
               IDC_MeasurementBtn.Location = new Point(
                   this.ClientSize.Width / 2 - this.IDC_MeasurementBtn.Location.X  ,
                   this.ClientSize.Height / 2 - this.IDC_MeasurementBtn.Location.Y);*/


        }

        private void IDD_BALANCELABO_DIALOG_Paint(object sender, PaintEventArgs e)
        {
            //Sumit need
            // e.Graphics.DrawRectangle(new Pen(Color.Black, 3),
            //   this.Location.X + 10, this.Location.Y +10, this.Width - 50, this.Height - 50);
            //e.Graphics.DrawLine(new Pen(Color.Black, 2), 0, 24, this.Width, 24);
                           // this.DisplayRectangle);

        }

        private void qRCODEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Image QRCode_Bitmap = ConvertToBitmap(m_pbyteQRCodeBitmapBits);
            if (qRCODEToolStripMenuItem.Checked)
            {
                GetDocument.SetQRCodeFlag(true);
            }
            else
            {
                GetDocument.SetQRCodeFlag(false);

            }
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();
                IDD_BALANCELABO m_Balancelabo_Dialog = new IDD_BALANCELABO(GetDocument);

                m_Balancelabo_Dialog.Width = panel2.Width;
                m_Balancelabo_Dialog.Height = panel2.Height;
                m_Balancelabo_Dialog.AutoScroll = true;

                m_Balancelabo_Dialog.TopLevel = false;

                panel2.BorderStyle = BorderStyle.Fixed3D;
                panel2.Controls.Add(m_Balancelabo_Dialog);
                panel2.Show();

                m_Balancelabo_Dialog.Show();
                m_Balancelabo_Dialog.closeForm += M_BALANCELABO_closeForm;
                m_Balancelabo_Dialog.OpenSettingScreen += M_BALANCELABO_OpenSettingScreen;

            
           

        }
        /// <summary>
        /// Converts to bitmap.
        /// </summary>
        /// <param name="imagesSource">The images source.</param>
        /// <returns>The bitmap.</returns>
        public Image ConvertToBitmap(byte[] imagesSource)
        {
            Image returnImage;
            //try
            {
                if(imagesSource == null)
                {
                    MessageBox.Show("no imge");
                }
                MemoryStream ms = new MemoryStream(imagesSource, 0, imagesSource.Length);
                ms.Write(imagesSource, 0, imagesSource.Length);
                 returnImage = Image.FromStream(ms, true);
                
            }
            //catch { }
            return returnImage;
        }

        private void qRCODEToolStripMenuItem_CheckStateChanged(object sender, EventArgs e)
        {
            
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
          
        }

        private void panel2_SizeChanged(object sender, EventArgs e)
        {
            // MessageBox.Show("for test");
            //panel2.Refresh();
        }

        private void lANGUAGESETTINGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var Language_form = new LanguageSetting(GetDocument);
            Language_form.Show();

            if (GetDocument.GetMeasurementViewMode() == Constants.MEASUREMENTVIEWMODE_INITIALIZE)
                Language_form.EventToLangChange += M_BALANCELABO_closeForm;
            else if (GetDocument.GetMeasurementStartViewMode() == Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN ||
                GetDocument.GetMeasurementStartViewMode() == Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING ||
                GetDocument.GetMeasurementStartViewMode() == Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING)
                Language_form.EventToLangChange += M_MeasurementDlg_EventToStartNextScreen;
            else if (GetDocument.GetJointEditViewMode() == Constants.JOINTEDITVIEWMODE_ANKLE_AND_HIP ||
                GetDocument.GetJointEditViewMode() == Constants.JOINTEDITVIEWMODE_KNEE ||
                GetDocument.GetJointEditViewMode() == Constants.JOINTEDITVIEWMODE_UPPERBODY)
                Language_form.EventToLangChange += M_MeasurementDlg_Closeformtostartnextscreen;
            else if (GetDocument.GetJointEditViewMode() == Constants.JOINTEDITVIEWMODE_SIDE)
                Language_form.EventToLangChange += M_JointEditView_EventToStartSideJointEditView;
            else if (GetDocument.GetFinalScreenMode() == Constants.FINAL_SCREEN_MODE_TRUE)
                Language_form.EventToLangChange += M_SideJointEditView_EventToChangeResultView;
            else if (GetDocument.GetSettingMode() == Constants.SETTING_SCREEN_MODE_TRUE)
                Language_form.EventToLangChange += M_BALANCELABO_OpenSettingScreen;
      
            else if (GetDocument.GetLanguage() != null)
                Language_form.EventToLangChange += SettingView_EventToGoInitialScreen;

          

        }

        private void pORTSETTINGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PortSetting port = new PortSetting();
            port.Show();
        }

        private void versionInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IDD_ABOUTBOX version = new IDD_ABOUTBOX();
            version.Show();

        }
        public void RefreshMenuStrip(bool EnableFlag)
        {
         
            Text = Properties.Resources.YUGAMIRU_TITLE;
            menuStrip1.Items[0].Text = Yugamiru.Properties.Resources.KEYWORD_SETTING;
            menuStrip1.Items[1].Text = Yugamiru.Properties.Resources.KEYWORD_HELP;
            this.qRCODEToolStripMenuItem.Text = Yugamiru.Properties.Resources.KEYWORD_QR;
            this.pORTSETTINGToolStripMenuItem.Text = Yugamiru.Properties.Resources.KEYWORD_PORT;
            this.versionInfoToolStripMenuItem.Text = Yugamiru.Properties.Resources.KEYWORD_VERSION;

            this.qRCODEToolStripMenuItem.Enabled = EnableFlag;
            this.pORTSETTINGToolStripMenuItem.Enabled = EnableFlag;
        }
        public void DisposeForms()
        {
            List<Form> openForms = new List<Form>();

            foreach (Form f in Application.OpenForms)
                openForms.Add(f);

            foreach (Form f in openForms)
            {
                if (f.Name != "IDD_BALANCELABO_DIALOG" && f.Name != "LanguageSetting")
                {
                    f.Controls.Clear();
                    f.Close();
                }

            }

        }

    }

  
}

