﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Yugamiru
{
    public class MuscleStatePatternDefinitionMgr
    {

        int m_iCount;
        MuscleStatePatternDefinitionElement[] m_pElement;
        SymbolFunc m_SymbolFunc = new SymbolFunc();

        // ‹Ø“÷ó‘Ôƒpƒ^[ƒ“’è‹`ƒ}ƒXƒ^[ƒf[ƒ^.
        public MuscleStatePatternDefinitionMgr()
        {
            m_iCount = 0;
            m_pElement = new MuscleStatePatternDefinitionElement[m_iCount];

        }


        public bool ReadFromFile(string lpszFolderName, string lpszFileName, string pchErrorFilePath /* = NULL */ )
        {
            char[] buf = new char[1024];
            List<MuscleStatePatternDefinitionElement> listElement = new List<MuscleStatePatternDefinitionElement>();

            if (m_pElement != null)
            {

                //m_pElement = null;
            }
            m_iCount = 0;

            DualSourceTextReader DualSourceTextReader = new DualSourceTextReader();
            int i = DualSourceTextReader.Open(lpszFolderName, lpszFileName);
            if (i == 0)
            {
                return false;
            }

            string strError = string.Empty;
            int iLineNo = 1;
            while (!(DualSourceTextReader.IsEOF()))
            {
                buf[0] = '\0';
                DualSourceTextReader.Read(ref buf, 1024);
                //var t = File.ReadAllLines(@"Resources\ResultAction.inf");
                MuscleStatePatternDefinitionElement Element = new MuscleStatePatternDefinitionElement();

                int iErrorCode = Element.ReadFromString(ref buf);
                if (iErrorCode == Constants.READFROMSTRING_SYNTAX_OK)
                {
                    //listElement.push_back(Element);
                    listElement.Add(Element);
                }
                else
                {
                    m_SymbolFunc.OutputReadErrorString(strError, iLineNo, iErrorCode);
                }
                iLineNo++;
            }


            // d•¡’è‹`ƒ`ƒFƒbƒN.
            //listElement.Sort();

            int iOldMuscleStatePatternID = Constants.ADULTMUSCLESTATEPATTERNID_NONE;
            int iOldMuscleID = Constants.MUSCLEID_NONE;
            bool bFoundMultipleDefinition = false;
            foreach (var index in listElement)
            {
                if ((index.GetMuscleStatePatternID() == iOldMuscleStatePatternID) && (index.GetMuscleID() == iOldMuscleID))
                {
                    string strTmp = string.Empty;
                    strTmp = "MultipleDefinition" + iOldMuscleStatePatternID + iOldMuscleID;
                    strError += strTmp;
                }
                iOldMuscleStatePatternID = index.GetMuscleStatePatternID();
                iOldMuscleID = index.GetMuscleID();
            }

            if (pchErrorFilePath != null)
            {
                StreamWriter sw = new StreamWriter(pchErrorFilePath);
                sw.Write(strError);

                sw.Dispose();
            }

            // €–Ú”•ª‚¾‚¯ƒƒ‚ƒŠŠm•Û.
            int iSize = listElement.Count;
            if (iSize <= 0)
            {
                return true;
            }
            m_pElement = new MuscleStatePatternDefinitionElement[iSize];
            if (m_pElement == null)
            {
                return false;
            }
            m_iCount = iSize;


            for (int j = 0; j < listElement.Count; j++)
            {
                m_pElement[j] = listElement[j];
            }

            return true;
        }

        public int GetElementCount()
        {
            return m_iCount;
        }

        public bool GetMuscleStateInfo(MuscleStateInfo MuscleStateInfo, int iMuscleStatePatternID, int iMuscleStateLevel)
        {
            bool bRet = false;
            int i = 0;
            for (i = 0; i < m_iCount; i++)
            {
                if (m_pElement[i].GetMuscleStateInfo(ref MuscleStateInfo, iMuscleStatePatternID, iMuscleStateLevel))
                {
                    bRet = true;
                }
            }
            return bRet;
        }

    }
}
