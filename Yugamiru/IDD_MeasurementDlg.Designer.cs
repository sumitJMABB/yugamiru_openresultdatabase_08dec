﻿namespace Yugamiru
{
    partial class IDD_MeasurementDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.IDC_NextBtn = new System.Windows.Forms.PictureBox();
            this.IDC_BackBtn = new System.Windows.Forms.PictureBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.IDC_COMBO_GENDER = new System.Windows.Forms.ComboBox();
            this.IDC_COMBO_MONTH = new System.Windows.Forms.ComboBox();
            this.IDC_COMBO_DAY = new System.Windows.Forms.ComboBox();
            this.IDC_BirthYear = new System.Windows.Forms.NumericUpDown();
            this.IDC_Height = new System.Windows.Forms.NumericUpDown();
            this.IDC_ID = new System.Windows.Forms.RichTextBox();
            this.IDC_Name = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_NextBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BackBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BirthYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Height)).BeginInit();
            this.SuspendLayout();
            // 
            // IDC_NextBtn
            // 
            this.IDC_NextBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_NextBtn.Location = new System.Drawing.Point(417, 404);
            this.IDC_NextBtn.Name = "IDC_NextBtn";
            this.IDC_NextBtn.Size = new System.Drawing.Size(100, 50);
            this.IDC_NextBtn.TabIndex = 0;
            this.IDC_NextBtn.TabStop = false;
            this.IDC_NextBtn.Click += new System.EventHandler(this.IDC_NextBtn_Click);
            // 
            // IDC_BackBtn
            // 
            this.IDC_BackBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_BackBtn.Location = new System.Drawing.Point(62, 404);
            this.IDC_BackBtn.Name = "IDC_BackBtn";
            this.IDC_BackBtn.Size = new System.Drawing.Size(100, 50);
            this.IDC_BackBtn.TabIndex = 1;
            this.IDC_BackBtn.TabStop = false;
            this.IDC_BackBtn.Click += new System.EventHandler(this.IDC_BackBtn_Click);
            // 
            // IDC_COMBO_GENDER
            // 
            this.IDC_COMBO_GENDER.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_COMBO_GENDER.DropDownHeight = 100;
            this.IDC_COMBO_GENDER.Font = new System.Drawing.Font("MS Reference Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDC_COMBO_GENDER.FormattingEnabled = true;
            this.IDC_COMBO_GENDER.IntegralHeight = false;
            this.IDC_COMBO_GENDER.Items.AddRange(new object[] {
            "-",
            "’j",
            "—"});
            this.IDC_COMBO_GENDER.Location = new System.Drawing.Point(298, 161);
            this.IDC_COMBO_GENDER.Name = "IDC_COMBO_GENDER";
            this.IDC_COMBO_GENDER.Size = new System.Drawing.Size(121, 37);
            this.IDC_COMBO_GENDER.TabIndex = 4;
            // 
            // IDC_COMBO_MONTH
            // 
            this.IDC_COMBO_MONTH.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_COMBO_MONTH.DropDownHeight = 100;
            this.IDC_COMBO_MONTH.Font = new System.Drawing.Font("MS Reference Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDC_COMBO_MONTH.FormattingEnabled = true;
            this.IDC_COMBO_MONTH.IntegralHeight = false;
            this.IDC_COMBO_MONTH.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.IDC_COMBO_MONTH.Location = new System.Drawing.Point(437, 221);
            this.IDC_COMBO_MONTH.Name = "IDC_COMBO_MONTH";
            this.IDC_COMBO_MONTH.Size = new System.Drawing.Size(38, 37);
            this.IDC_COMBO_MONTH.TabIndex = 5;
            // 
            // IDC_COMBO_DAY
            // 
            this.IDC_COMBO_DAY.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_COMBO_DAY.DropDownHeight = 100;
            this.IDC_COMBO_DAY.Font = new System.Drawing.Font("MS Reference Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.IDC_COMBO_DAY.FormattingEnabled = true;
            this.IDC_COMBO_DAY.IntegralHeight = false;
            this.IDC_COMBO_DAY.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.IDC_COMBO_DAY.Location = new System.Drawing.Point(499, 221);
            this.IDC_COMBO_DAY.Name = "IDC_COMBO_DAY";
            this.IDC_COMBO_DAY.Size = new System.Drawing.Size(38, 40);
            this.IDC_COMBO_DAY.TabIndex = 6;
            // 
            // IDC_BirthYear
            // 
            this.IDC_BirthYear.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_BirthYear.Font = new System.Drawing.Font("MS Reference Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDC_BirthYear.Location = new System.Drawing.Point(298, 221);
            this.IDC_BirthYear.Maximum = new decimal(new int[] {
            2016,
            0,
            0,
            0});
            this.IDC_BirthYear.Name = "IDC_BirthYear";
            this.IDC_BirthYear.Size = new System.Drawing.Size(120, 37);
            this.IDC_BirthYear.TabIndex = 7;
            this.IDC_BirthYear.Value = new decimal(new int[] {
            1980,
            0,
            0,
            0});
            this.IDC_BirthYear.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // IDC_Height
            // 
            this.IDC_Height.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_Height.Font = new System.Drawing.Font("MS Reference Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDC_Height.Location = new System.Drawing.Point(299, 291);
            this.IDC_Height.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.IDC_Height.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            65536});
            this.IDC_Height.Name = "IDC_Height";
            this.IDC_Height.Size = new System.Drawing.Size(120, 37);
            this.IDC_Height.TabIndex = 8;
            this.IDC_Height.Value = new decimal(new int[] {
            1000,
            0,
            0,
            65536});
            // 
            // IDC_ID
            // 
            this.IDC_ID.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_ID.Font = new System.Drawing.Font("MS Reference Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.IDC_ID.Location = new System.Drawing.Point(298, 40);
            this.IDC_ID.Name = "IDC_ID";
            this.IDC_ID.Size = new System.Drawing.Size(93, 42);
            this.IDC_ID.TabIndex = 9;
            this.IDC_ID.Text = "";
            // 
            // IDC_Name
            // 
            this.IDC_Name.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_Name.Font = new System.Drawing.Font("MS Reference Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.IDC_Name.Location = new System.Drawing.Point(298, 98);
            this.IDC_Name.Name = "IDC_Name";
            this.IDC_Name.Size = new System.Drawing.Size(93, 42);
            this.IDC_Name.TabIndex = 10;
            this.IDC_Name.Text = "";
            // 
            // IDD_MeasurementDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(631, 504);
            this.Controls.Add(this.IDC_Name);
            this.Controls.Add(this.IDC_ID);
            this.Controls.Add(this.IDC_Height);
            this.Controls.Add(this.IDC_BirthYear);
            this.Controls.Add(this.IDC_COMBO_DAY);
            this.Controls.Add(this.IDC_COMBO_MONTH);
            this.Controls.Add(this.IDC_COMBO_GENDER);
            this.Controls.Add(this.IDC_BackBtn);
            this.Controls.Add(this.IDC_NextBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IDD_MeasurementDlg";
            this.Text = "IDD_MeasurementDlg";
            this.Load += new System.EventHandler(this.IDD_MeasurementDlg_Load);
            this.SizeChanged += new System.EventHandler(this.IDD_MeasurementDlg_SizeChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.IDD_MeasurementDlg_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.IDC_NextBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BackBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BirthYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Height)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox IDC_NextBtn;
        private System.Windows.Forms.PictureBox IDC_BackBtn;
        private System.Windows.Forms.BindingSource bindingSource1;
        public System.Windows.Forms.ComboBox IDC_COMBO_GENDER;
        public System.Windows.Forms.ComboBox IDC_COMBO_MONTH;
        public System.Windows.Forms.ComboBox IDC_COMBO_DAY;
        public System.Windows.Forms.NumericUpDown IDC_BirthYear;
        public System.Windows.Forms.NumericUpDown IDC_Height;
        public System.Windows.Forms.RichTextBox IDC_ID;
        public System.Windows.Forms.RichTextBox IDC_Name;
    }
}