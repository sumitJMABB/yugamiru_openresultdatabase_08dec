﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
   public class SettingDataElement
    {
       public string m_strVariableName;  // •Ï”–¼.
       public string m_strValue;			// •Ï”‚Ì’l.

        // Žp¨ƒpƒ^[ƒ“”»’èÝ’èƒf[ƒ^‚ÌƒXƒL[ƒ}.
        public SettingDataElement()
        {
            m_strVariableName = string.Empty;

            m_strValue = string.Empty;
        
        }

       
    public bool ReadFromString(char[] lpszText)
    {
        string strVariableName = string.Empty ;
        string strValue = string.Empty;

        TextBuffer TextBuffer = new TextBuffer(lpszText);

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadSymbol(ref strVariableName))
        {
            return (false);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadComma())
        {
            return (false);
        }

        TextBuffer.SkipSpaceOrTab();
        if (!TextBuffer.ReadString(ref strValue))
        {
            return (false);
        }

        m_strVariableName = strVariableName;
        m_strValue = strValue;

        return (true);
    }

}
}
