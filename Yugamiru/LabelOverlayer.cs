﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Yugamiru
{
    public class LabelOverlayer
    {
        string[] m_astrText = new string[Constants.LABELOVERLAYER_POSITION_MAX];

        int[,] ms_aaiLabelPosType = new int[Constants.LABELOVERLAYER_POSITION_MAX,2]
            {
            { Constants.LABELOVERLAYER_X_TYPE_0,    Constants.LABELOVERLAYER_Y_TYPE_0 },//LABELOVERLAYER_POSITION_00
	{ Constants.LABELOVERLAYER_X_TYPE_0,    Constants.LABELOVERLAYER_Y_TYPE_1 },//LABELOVERLAYER_POSITION_01
	{ Constants.LABELOVERLAYER_X_TYPE_1,    Constants.LABELOVERLAYER_Y_TYPE_0 },//LABELOVERLAYER_POSITION_02
	{ Constants.LABELOVERLAYER_X_TYPE_1,    Constants.LABELOVERLAYER_Y_TYPE_1 },//LABELOVERLAYER_POSITION_03
	{ Constants.LABELOVERLAYER_X_TYPE_0,    Constants.LABELOVERLAYER_Y_TYPE_2 },//LABELOVERLAYER_POSITION_04
	{ Constants.LABELOVERLAYER_X_TYPE_0,    Constants.LABELOVERLAYER_Y_TYPE_3 },//LABELOVERLAYER_POSITION_05
	{ Constants.LABELOVERLAYER_X_TYPE_1,    Constants.LABELOVERLAYER_Y_TYPE_2 },//LABELOVERLAYER_POSITION_06
	{ Constants.LABELOVERLAYER_X_TYPE_1,    Constants.LABELOVERLAYER_Y_TYPE_3 },//LABELOVERLAYER_POSITION_07
	{ Constants.LABELOVERLAYER_X_TYPE_0,    Constants.LABELOVERLAYER_Y_TYPE_4 },//LABELOVERLAYER_POSITION_08
	{ Constants.LABELOVERLAYER_X_TYPE_0,    Constants.LABELOVERLAYER_Y_TYPE_5 },//LABELOVERLAYER_POSITION_09
	{ Constants.LABELOVERLAYER_X_TYPE_1,    Constants.LABELOVERLAYER_Y_TYPE_4 },//LABELOVERLAYER_POSITION_10
	{ Constants.LABELOVERLAYER_X_TYPE_1,    Constants.LABELOVERLAYER_Y_TYPE_5 },//LABELOVERLAYER_POSITION_11
	{ Constants.LABELOVERLAYER_X_TYPE_0,    Constants.LABELOVERLAYER_Y_TYPE_6 },//LABELOVERLAYER_POSITION_12
	{ Constants.LABELOVERLAYER_X_TYPE_0,    Constants.LABELOVERLAYER_Y_TYPE_7 },//LABELOVERLAYER_POSITION_13
	{ Constants.LABELOVERLAYER_X_TYPE_1,    Constants.LABELOVERLAYER_Y_TYPE_6 },//LABELOVERLAYER_POSITION_14
	{ Constants.LABELOVERLAYER_X_TYPE_1,    Constants.LABELOVERLAYER_Y_TYPE_7 },//LABELOVERLAYER_POSITION_15
	{ Constants.LABELOVERLAYER_X_TYPE_0,    Constants.LABELOVERLAYER_Y_TYPE_8 },//LABELOVERLAYER_POSITION_16
	{ Constants.LABELOVERLAYER_X_TYPE_0,    Constants.LABELOVERLAYER_Y_TYPE_9 },//LABELOVERLAYER_POSITION_17
	{ Constants.LABELOVERLAYER_X_TYPE_1,    Constants.LABELOVERLAYER_Y_TYPE_8 },//LABELOVERLAYER_POSITION_18
	{ Constants.LABELOVERLAYER_X_TYPE_1,    Constants.LABELOVERLAYER_Y_TYPE_9 },//LABELOVERLAYER_POSITION_19
	{ Constants.LABELOVERLAYER_X_TYPE_0,    Constants.LABELOVERLAYER_Y_TYPE_10 },//LABELOVERLAYER_POSITION_20
	{ Constants.LABELOVERLAYER_X_TYPE_0,    Constants.LABELOVERLAYER_Y_TYPE_11 },//LABELOVERLAYER_POSITION_21
	{ Constants.LABELOVERLAYER_X_TYPE_1,    Constants.LABELOVERLAYER_Y_TYPE_10 },//LABELOVERLAYER_POSITION_22
	{ Constants.LABELOVERLAYER_X_TYPE_1,    Constants.LABELOVERLAYER_Y_TYPE_11 },//LABELOVERLAYER_POSITION_23
};
        public LabelOverlayer( )
{
}

public LabelOverlayer( LabelOverlayer rSrc )
{
	int i = 0; 
	for( i = 0; i<Constants.LABELOVERLAYER_POSITION_MAX; i++ ){
		m_astrText[i] = rSrc.m_astrText[i];
	}
}
   public      void SetString(int iLabelPosID, string pchText )
{
	if ( iLabelPosID< 0 ){
		return;
	}
	if ( iLabelPosID >= Constants.LABELOVERLAYER_POSITION_MAX ){
		return;
	}
	m_astrText[iLabelPosID] = pchText;
}
     public void SetValue(int iLabelPosID, float fValue)
        {
            if (iLabelPosID < 0)
            {
                return;
            }
            if (iLabelPosID >= Constants.LABELOVERLAYER_POSITION_MAX)
            {
                return;
            }
            m_astrText[iLabelPosID] = fValue.ToString();
        }

        public bool GetLabelPosition(Point ptDest, Rectangle rcDraw, Size sizeCharacter, string pchText, int iLablePositionID)
        {

            ptDest.X = 0;
            ptDest.Y = 0;
            if (iLablePositionID < 0)
            {
                return false;
            }
            if (iLablePositionID >= Constants.LABELOVERLAYER_POSITION_MAX)
            {
                return false;
            }
            int iTypeOfX = ms_aaiLabelPosType[iLablePositionID, 0];
            int iTypeOfY = ms_aaiLabelPosType[iLablePositionID, 1];
            int iXPosition = 0;
            int iYPosition = 0;
            switch (iTypeOfX)
            {
                case Constants.LABELOVERLAYER_X_TYPE_0:
                    iXPosition = rcDraw.Left + sizeCharacter.Width * 2;
                    break;
                case Constants.LABELOVERLAYER_X_TYPE_1:
                    iXPosition = rcDraw.Right - sizeCharacter.Width * 2 - (sizeCharacter.Width * pchText.Length);
                    break;
                default:
                    break;
            }
            int iRectOffset = rcDraw.Top + sizeCharacter.Height;
            int iRectValidHeight = rcDraw.Bottom - rcDraw.Top - sizeCharacter.Height * 2;

            switch (iTypeOfY)
            {
                case Constants.LABELOVERLAYER_Y_TYPE_0:
                    iYPosition = iRectOffset + (sizeCharacter.Height / 2) - (sizeCharacter.Height);
                    break;
                case Constants.LABELOVERLAYER_Y_TYPE_1:
                    iYPosition = iRectOffset + (sizeCharacter.Height / 2) + (sizeCharacter.Height);
                    break;
                case Constants.LABELOVERLAYER_Y_TYPE_2:
                    iYPosition = iRectOffset + iRectValidHeight * 1 / 5 - (sizeCharacter.Height);
                    break;
                case Constants.LABELOVERLAYER_Y_TYPE_3:
                    iYPosition = iRectOffset + iRectValidHeight * 1 / 5 + (sizeCharacter.Height);
                    break;
                case Constants.LABELOVERLAYER_Y_TYPE_4:
                    iYPosition = iRectOffset + iRectValidHeight * 2 / 5 - (sizeCharacter.Height);
                    break;
                case Constants.LABELOVERLAYER_Y_TYPE_5:
                    iYPosition = iRectOffset + iRectValidHeight * 2 / 5 + (sizeCharacter.Height);
                    break;
                case Constants.LABELOVERLAYER_Y_TYPE_6:
                    iYPosition = iRectOffset + iRectValidHeight * 3 / 5 - (sizeCharacter.Height);
                    break;
                case Constants.LABELOVERLAYER_Y_TYPE_7:
                    iYPosition = iRectOffset + iRectValidHeight * 3 / 5 + (sizeCharacter.Height);
                    break;
                case Constants.LABELOVERLAYER_Y_TYPE_8:
                    iYPosition = iRectOffset + iRectValidHeight * 4 / 5 - (sizeCharacter.Height);
                    break;
                case Constants.LABELOVERLAYER_Y_TYPE_9:
                    iYPosition = iRectOffset + iRectValidHeight * 4 / 5 + (sizeCharacter.Height);
                    break;
                case Constants.LABELOVERLAYER_Y_TYPE_10:
                    iYPosition = iRectOffset + iRectValidHeight - (sizeCharacter.Height * 3 / 2) - (sizeCharacter.Height);
                    break;
                case Constants.LABELOVERLAYER_Y_TYPE_11:
                    iYPosition = iRectOffset + iRectValidHeight - (sizeCharacter.Height * 3 / 2) + (sizeCharacter.Height);
                    break;
                default:
                    break;
            }
            ptDest.X = iXPosition;
            ptDest.Y = iYPosition;
            return (true);
        }
/*  public void MyDrawText(Graphics pDC, string pchText,

                Point ptTopLeft, Color crText,
                bool bOutline, Color crOutline )
{

    int iTextLength = pchText.Length;

        Size sizeCharacter = new Size();
	//::GetTextExtentPoint32(pDC->GetSafeHdc(), pchText, iTextLength, &sizeCharacter );

	int iOffset = 0;
	if ( bOutline ){
		iOffset = 2;
	}
    Point ptBottomRight = new Point(0,0);
    ptBottomRight.X = ptTopLeft.X + sizeCharacter.Width + iOffset;
	ptBottomRight.Y = ptTopLeft.Y + sizeCharacter.Height + iOffset;

	Rectangle rcDraw = new Rectangle(ptTopLeft.X,ptTopLeft.Y,(ptBottomRight.X - ptTopLeft.X),
      (ptBottomRight.Y - ptTopLeft.Y));
    //rcDraw.SetRect( ptTopLeft, ptBottomRight );

	//int iBkModeOld = pDC->SetBkMode(TRANSPARENT); //ê‡‚É‚æ‚è

    int iCharacterWidth = sizeCharacter.Height / 12;
	if ( iCharacterWidth< 1 ){
		iCharacterWidth = 1;
	}
int iOutlineWidth = iCharacterWidth * 2;
int iTextWidth = 1; //iCharacterWidth - 1;
	if ( iTextWidth< 1 ){
		iTextWidth = 1;
	}
	Color crTextColorOld =  Color.FromArgb(0, 0, 0);
	if ( bOutline ){
		//ã‰º¶‰E‚É‚¸‚ç‚µ‚ÄA—ÖŠs‚ð•`‰æ
		//crTextColorOld = pDC.SetTextColor(crOutline);
int i = 0;
		for( i = 0; i<iOutlineWidth; i++ ){
			Rectangle rcOutline;
rcOutline.SetRect( rcDraw.TopLeft().x,    rcDraw.TopLeft().y - i, rcDraw.BottomRight().x,rcDraw.BottomRight().y);	//ã
			pDC.DrawString(pchText, -1, &rcOutline, DT_WORDBREAK);
rcOutline.SetRect( rcDraw.TopLeft().x - i,rcDraw.TopLeft().y,     rcDraw.BottomRight().x,rcDraw.BottomRight().y);	//¶
			pDC->DrawText(pchText, -1, &rcOutline, DT_WORDBREAK);
rcOutline.SetRect( rcDraw.TopLeft().x + i,rcDraw.TopLeft().y,     rcDraw.BottomRight().x,rcDraw.BottomRight().y);	//‰E
			pDC->DrawText(pchText, -1, &rcOutline, DT_WORDBREAK);
rcOutline.SetRect( rcDraw.TopLeft().x,    rcDraw.TopLeft().y + i, rcDraw.BottomRight().x,rcDraw.BottomRight().y);	//‰º
			pDC->DrawText(pchText, -1, &rcOutline, DT_WORDBREAK);
		}
		pDC->SetTextColor(crTextColorOld);
	}
	{
		crTextColorOld = pDC->SetTextColor(crText);
int i = 0;
		for( i = 0; i<iTextWidth; i++ ){
			CRect rcOutline;
rcOutline.SetRect( rcDraw.TopLeft().x,    rcDraw.TopLeft().y - i, rcDraw.BottomRight().x,rcDraw.BottomRight().y);	//ã
			pDC->DrawText(pchText, -1, &rcOutline, DT_WORDBREAK);
rcOutline.SetRect( rcDraw.TopLeft().x - i,rcDraw.TopLeft().y,     rcDraw.BottomRight().x,rcDraw.BottomRight().y);	//¶
			pDC->DrawText(pchText, -1, &rcOutline, DT_WORDBREAK);
rcOutline.SetRect( rcDraw.TopLeft().x + i,rcDraw.TopLeft().y,     rcDraw.BottomRight().x,rcDraw.BottomRight().y);	//‰E
			pDC->DrawText(pchText, -1, &rcOutline, DT_WORDBREAK);
rcOutline.SetRect( rcDraw.TopLeft().x,    rcDraw.TopLeft().y + i, rcDraw.BottomRight().x,rcDraw.BottomRight().y);	//‰º
			pDC->DrawText(pchText, -1, &rcOutline, DT_WORDBREAK);
		}		
		pDC->SetTextColor(crTextColorOld );
	}	
	pDC->SetBkMode(iBkModeOld );
}
*/

    }
}