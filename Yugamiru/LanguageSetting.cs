﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Resources;
using Yugamiru.Properties;
using System.Collections;

namespace Yugamiru
{
    public partial class LanguageSetting : Form
    {
        JointEditDoc m_JointEditDoc = new JointEditDoc();
    


        public LanguageSetting(JointEditDoc GetDocument)
        {
            InitializeComponent();
            if(GetDocument.GetLanguage() != string.Empty)
            m_Combo_Language.SelectedIndex = m_Combo_Language.Items.IndexOf(GetDocument.GetLanguage());
            else
            m_Combo_Language.SelectedIndex = 0;
            m_JointEditDoc = GetDocument;
        
        }

        private void LanguageSetting_Load(object sender, EventArgs e)
        {

        }

        private void ID_OK_Click(object sender, EventArgs e)
        {
            
            var selectedLanguage = m_Combo_Language.SelectedItem.ToString();
            Yugamiru.Properties.Resources.ResourceManager.ReleaseAllResources();
            switch (selectedLanguage)
                            {
                case "Japanese":                 
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("ja-JP");                  
                    break;
                case "Thai":
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("th-TH");
                    break;
                case "Korean":
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("ko-KR");
                    break;
                case "Chinese":
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("zh-CN");
                    break;
                default:
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("en");
                    break;
            }
            var t = CultureInfo.DefaultThreadCurrentUICulture;

            //Reload all the controls
            m_JointEditDoc.SetLanguage(selectedLanguage);
           // showRecords languageChange = new showRecords( );

            
           // languageChange.japaneseDatabaseLang();
            //Finally refresh the parent form
            FunctiontoChangeLanguage(EventArgs.Empty);
           // var showLang = new IDD_BALANCELABO_DIALOG();
           // EventToLangChange += showLang.open_ResultScreen;
            this.Close();
            

        }

        private void ID_Cancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
            this.Close();
        }
        public event EventHandler EventToLangChange; // creating event handler - step1
        public void FunctiontoChangeLanguage(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToLangChange;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }
    }
}
