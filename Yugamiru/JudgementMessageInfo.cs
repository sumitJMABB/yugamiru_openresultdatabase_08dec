﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class JudgementMessageInfo
    {

        List<int> m_listPelvis = new List<int>();
        List<int> m_listRightLeg = new List<int>();
        List<int> m_listLeftLeg = new List<int>();
        List<int> m_listBothLeg = new List<int>();
        List<int> m_listShoulder = new List<int>();
        List<int> m_listNeck = new List<int>();
        
        /*static*/ string[]  s_apchJudgementMessageDetail = new string[Constants.JUDGEMENT_MESSAGEID_MAX]  {
    Yugamiru.Properties.Resources.MUSCLEREPORT1/*"muscles around hip joint are well controlled."*/,
    Yugamiru.Properties.Resources.MUSCLEREPORT2/*"muscles in front of right thigh and muscles around right hip joint are strained. or muscles around right hip and abdominal muscle pressure are not controlled."*/,
    Yugamiru.Properties.Resources.MUSCLEREPORT3/*"muscles in front of left thigh and muscles around left hip joint are strained. or muscles around left hip and abdominal muscle pressure are not controlled."*/,
    Yugamiru.Properties.Resources.MUSCLEREPORT4/*"muscles on the outside of right hip are strained. the outside of left hip joint is not controlled."*/,
    Yugamiru.Properties.Resources.MUSCLEREPORT5/*"muscles on the outside of left hip are strained. the outside of right hip joint is not controlled."*/,
    Yugamiru.Properties.Resources.MUSCLEREPORT6/*"well balanced."*/,

    Yugamiru.Properties.Resources.MUSCLEREPORT7/*"iliotibial tract, lateral head of gastrocnemius, peroneal muscle, hip joint adductors are strained. quadriceps femoris is not controlled. mobility of cuboid is low."*/,
    Yugamiru.Properties.Resources.MUSCLEREPORT8/*"mobility of muscles around joint capsule in the back of hip joint is low. iliotibial tract, lateral head of gastrocnemius are strained. quadriceps femoris and gluteus medius are not controlled."*/,
    Yugamiru.Properties.Resources.MUSCLEREPORT6/*"well balanced."*/,

    Yugamiru.Properties.Resources.MUSCLEREPORT7/*"iliotibial tract, lateral head of gastrocnemius, peroneal muscle, hip joint adductors are strained. quadriceps femoris is not controlled. mobility of cuboid is low."*/,
    Yugamiru.Properties.Resources.MUSCLEREPORT8/*"mobility of muscles around joint capsule in the back of hip joint is low. iliotibial tract, lateral head of gastrocnemius are strained. quadriceps femoris and gluteus medius are not controlled."*/,

    Yugamiru.Properties.Resources.MUSCLEREPORT6/*"well balanced."*/,

    Yugamiru.Properties.Resources.MUSCLEREPORT7/*"iliotibial tract, lateral head of gastrocnemius, peroneal muscle, hip joint adductors are strained. quadriceps femoris is not controlled. mobility of cuboid is low."*/,
    Yugamiru.Properties.Resources.MUSCLEREPORT8/*"mobility of muscles around joint capsule in the back of hip joint is low. iliotibial tract, lateral head of gastrocnemius are strained. quadriceps femoris and gluteus medius are not controlled."*/,
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
};

        /*static*/ string[] s_apchJudgementMessageNormal = new string[Constants.JUDGEMENT_MESSAGEID_MAX]  {
    Yugamiru.Properties.Resources.MUSCLEREPORT1,//"muscles around hip joint are well controlled.",
    Yugamiru.Properties.Resources.MUSCLEREPORT2,//"muscles in front of right thigh and muscles around right hip joint are strained. or muscles around right hip and abdominal muscle pressure are not controlled.",
    Yugamiru.Properties.Resources.MUSCLEREPORT3,//"muscles in front of left thigh and muscles around left hip joint are strained. or muscles around left hip and abdominal muscle pressure are not controlled.",
    Yugamiru.Properties.Resources.MUSCLEREPORT4,//"muscles on the outside of right thigh are strained. muscles on the outside of left thigh are not controlled.",
    Yugamiru.Properties.Resources.MUSCLEREPORT5,//"muscles on the outside of left thigh are strained. muscles on the outside of right thigh are not controlled.",
    Yugamiru.Properties.Resources.MUSCLEREPORT6,//"well balanced.",
    Yugamiru.Properties.Resources.MUSCLEREPORT11,//"muscles on the outside of both knees and calves and muscles around inner thigh are strained. the lower and inner part of thigh is not controlled. mobility of arch of the foot is low.",
    Yugamiru.Properties.Resources.MUSCLEREPORT12,//"mobilty of joints in the back of both hip joints is low. muscles on the outside of  both knees and upper calves are strained. muscles on the outside of lower thigh, inner thigh, hip are not controlled. ",
    Yugamiru.Properties.Resources.MUSCLEREPORT6,//"well balanced.",
    Yugamiru.Properties.Resources.MUSCLEREPORT13,//"muscles on the outside of right knee and calve and muscles around inner thigh are strained. the lower and inner part of thigh is not controlled. mobility of arch of the foot is low.",
    Yugamiru.Properties.Resources.MUSCLEREPORT14,//"mobilty of joints in the back of right hip joint is low. muscles on the outside of right knee and upper calve are strained. muscles on the outside of lower thigh, inner thigh, hip are not controlled. ",
    Yugamiru.Properties.Resources.MUSCLEREPORT6,//"well balanced.",
    Yugamiru.Properties.Resources.MUSCLEREPORT15,//"muscles on the outside of left knee and calve and muscles around inner thigh are strained. the lower and inner part of thigh is not controlled. mobility of arch of the foot is low.",
    Yugamiru.Properties.Resources.MUSCLEREPORT16,//"mobilty of joints in the back of left hip joint is low. muscles on the outside of left knee and upper calve are strained. muscles on the outside of lower thigh, inner thigh, hip are not controlled. ",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
};

        /*static*/ string[] s_apchMessagePartTitle = new string[Constants.JUDGEMENT_MESSAGEPARTID_MAX]  {
    /*"HIP"*/Yugamiru.Properties.Resources.HIP,
    /*"KNEE"*/Yugamiru.Properties.Resources.CAPS_KNEE,
    /*"RIGHT LEG"*/Yugamiru.Properties.Resources.RIGHTLEG,
    /*"LEFT LEG"*/Yugamiru.Properties.Resources.LEFTLEG,
    /*"SHOULDER"*/Yugamiru.Properties.Resources.CAPS_SHOULDER,
    /*"NECK"*/Yugamiru.Properties.Resources.NECK,
};

        public/*static*/ void GetMessagePartTitleString(ref string strTitle, string pchTitleBase, int iMessagePartID )
{

    strTitle = string.Empty;
	if ( iMessagePartID< 0 ){
		return;
	}
	if ( iMessagePartID >= Constants.JUDGEMENT_MESSAGEPARTID_MAX ){
		return;
	}
	strTitle = "["+pchTitleBase + s_apchMessagePartTitle[iMessagePartID]+ " "+Yugamiru.Properties.Resources.STATUS+"]\r\n";
}

    /*static*/public void GetJudgementMessageString(ref string strMessage, bool bDetail, int iMessageID)
    {
        strMessage = string.Empty;
        if (iMessageID < 0)
        {
            return;
        }
        if (iMessageID >= Constants.JUDGEMENT_MESSAGEID_MAX)
        {
            return;
        }
        if (bDetail)
        {
            strMessage = s_apchJudgementMessageDetail[iMessageID];
        }
        else
        {
            strMessage = s_apchJudgementMessageNormal[iMessageID];
        }
    }

    public JudgementMessageInfo()
        { 

    }

    public JudgementMessageInfo( JudgementResult JudgementResult )
        { 
    
        SetJudgementResult(JudgementResult);
    }

 

public void AddPelvisMessage(int iMessageID)
{
    m_listPelvis.Add(iMessageID);
}

public void AddRightLegMessage(int iMessageID)
{
    m_listRightLeg.Add(iMessageID);
}

public void AddLeftLegMessage(int iMessageID)
{
    m_listLeftLeg.Add(iMessageID);
}

public void AddBothLegMessage(int iMessageID)
{
    m_listBothLeg.Add(iMessageID);
}

public void AddShoulderMessage(int iMessageID)
{
    m_listShoulder.Add(iMessageID);
}

public void AddNeckMessage(int iMessageID)
{
    m_listNeck.Add(iMessageID);
}

public void SetJudgementResult( JudgementResult JudgementResult )
{
    if ((JudgementResult.GetPelvicRotate() == 0) && (JudgementResult.GetPelvicTilt() == 0))
    {
        AddPelvisMessage(Constants.JUDGEMENT_MESSAGEID_PELVIS_NORMAL);
    }
    else
    {
        if (JudgementResult.GetPelvicRotate() < 0)
        {
            AddPelvisMessage(Constants.JUDGEMENT_MESSAGEID_PELVIS_ROTATE_LEFT);
        }
        else if (JudgementResult.GetPelvicRotate() > 0)
        {
            AddPelvisMessage(Constants.JUDGEMENT_MESSAGEID_PELVIS_ROTATE_RIGHT);
        }
        if (JudgementResult.GetPelvicTilt() < 0)
        {
            AddPelvisMessage(Constants.JUDGEMENT_MESSAGEID_PELVIS_TILT_LEFT);
        }
        else if (JudgementResult.GetPelvicTilt() > 0)
        {
            AddPelvisMessage(Constants.JUDGEMENT_MESSAGEID_PELVIS_TILT_RIGHT);
        }
    }

    if ((JudgementResult.GetRightLeg() == 0) && (JudgementResult.GetLeftLeg() == 0))
    {
        AddBothLegMessage(Constants.JUDGEMENT_MESSAGEID_BOTH_LEG_NORMAL);
    }
    else if ((JudgementResult.GetRightLeg() < 0) && (JudgementResult.GetLeftLeg() < 0))
    {
        AddBothLegMessage(Constants.JUDGEMENT_MESSAGEID_BOTH_LEG_CROSSLEGGED);
    }
    else if ((JudgementResult.GetRightLeg() > 0) && (JudgementResult.GetLeftLeg() > 0))
    {
        AddBothLegMessage(Constants.JUDGEMENT_MESSAGEID_BOTH_LEG_BANDLYLEGGED);
    }
    else
    {
        if (JudgementResult.GetRightLeg() < 0)
        {
            AddRightLegMessage(Constants.JUDGEMENT_MESSAGEID_RIGHT_LEG_CROSSLEGGED);
        }
        else if (JudgementResult.GetRightLeg() > 0)
        {
            AddRightLegMessage(Constants.JUDGEMENT_MESSAGEID_RIGHT_LEG_BANDLYLEGGED);
        }
        else
        {
            AddRightLegMessage(Constants.JUDGEMENT_MESSAGEID_RIGHT_LEG_NORMAL);
        }

        if (JudgementResult.GetLeftLeg() < 0)
        {
            AddLeftLegMessage(Constants.JUDGEMENT_MESSAGEID_LEFT_LEG_CROSSLEGGED);
        }
        else if (JudgementResult.GetLeftLeg() > 0)
        {
            AddLeftLegMessage(Constants.JUDGEMENT_MESSAGEID_LEFT_LEG_BANDLYLEGGED);
        }
        else
        {
            AddLeftLegMessage(Constants.JUDGEMENT_MESSAGEID_LEFT_LEG_NORMAL);
        }
    }
/*
#if defined(_NECK_SHOULDER)
	if ( JudgementResult.GetShoulder() == -2 ){
		AddShoulderMessage( JUDGEMENT_MESSAGEID_SHOULDER_TILT_RIGHT_LEVEL2 );
	}
	else if ( JudgementResult.GetShoulder() == -1 ){
		AddShoulderMessage( JUDGEMENT_MESSAGEID_SHOULDER_TILT_RIGHT_LEVEL1 );
	}
	else if ( JudgementResult.GetShoulder() == 1 ){
		AddShoulderMessage( JUDGEMENT_MESSAGEID_SHOULDER_TILT_LEFT_LEVEL1 );
	}
	else if ( JudgementResult.GetShoulder() == 2 ){
		AddShoulderMessage( JUDGEMENT_MESSAGEID_SHOULDER_TILT_LEFT_LEVEL2 );
	}
	else{
		AddShoulderMessage( JUDGEMENT_MESSAGEID_SHOULDER_NORMAL );
	}

	if ( JudgementResult.GetNeck() == -2 ){
		AddNeckMessage( JUDGEMENT_MESSAGEID_NECK_TILT_RIGHT_LEVEL2 );
	}
	else if ( JudgementResult.GetNeck() == -1 ){
		AddNeckMessage( JUDGEMENT_MESSAGEID_NECK_TILT_RIGHT_LEVEL1 );
	}
	else if ( JudgementResult.GetNeck() == 1 ){
		AddNeckMessage( JUDGEMENT_MESSAGEID_NECK_TILT_LEFT_LEVEL1 );
	}
	else if ( JudgementResult.GetNeck() == 2 ){
		AddNeckMessage( JUDGEMENT_MESSAGEID_NECK_TILT_LEFT_LEVEL2 );
	}
	else{
		AddNeckMessage( JUDGEMENT_MESSAGEID_NECK_NORMAL );
	}
#endif
*/
}

public void MakeString(ref string strJudge, string pchTitle, bool bDetail)
{
    strJudge = string.Empty;
    string strMessage = string.Empty;

    //std::list<int>::const_iterator it;
    if (!(m_listPelvis.Count == 0))
    {
        GetMessagePartTitleString(ref strMessage, pchTitle, Constants.JUDGEMENT_MESSAGEPARTID_PELVIS);
        strJudge += strMessage;
        for (int i =0; i < m_listPelvis.Count; i++)
        {
            GetJudgementMessageString(ref strMessage, bDetail, m_listPelvis[i]);
            strJudge += strMessage;
            strJudge += "\r\n";
        }
        strJudge += "\r\n";
    }
    if (!(m_listRightLeg.Count == 0))
    {
        GetMessagePartTitleString(ref strMessage, pchTitle, Constants.JUDGEMENT_MESSAGEPARTID_RIGHT_LEG);
        strJudge += strMessage;
        for (int i = 0; i< m_listRightLeg.Count; i++)
        {
            GetJudgementMessageString(ref strMessage, bDetail, m_listRightLeg[i]);
            strJudge += strMessage;
            strJudge += "\r\n";
        }
        strJudge += "\r\n";
    }

    if (!(m_listLeftLeg.Count == 0))
    {
        GetMessagePartTitleString(ref strMessage, pchTitle, Constants.JUDGEMENT_MESSAGEPARTID_LEFT_LEG);
        strJudge += strMessage;
        for (int i = 0; i< m_listLeftLeg.Count; i++)
        {
            GetJudgementMessageString(ref strMessage, bDetail, m_listLeftLeg[i]);
            strJudge += strMessage;
            strJudge += "\r\n";
        }
        strJudge += "\r\n";
    }

    if (!(m_listBothLeg.Count == 0))
    {
        GetMessagePartTitleString(ref strMessage, pchTitle, Constants.JUDGEMENT_MESSAGEPARTID_BOTH_LEG);
        strJudge += strMessage;
        for (int i = 0; i < m_listBothLeg.Count; i++)
        {
            GetJudgementMessageString(ref strMessage, bDetail, m_listBothLeg[i]);
            strJudge += strMessage;
            strJudge += "\r\n";
        }
        strJudge += "\r\n";
    }

    if (!(m_listShoulder.Count == 0))
    {
        GetMessagePartTitleString(ref strMessage, pchTitle, Constants.JUDGEMENT_MESSAGEPARTID_SHOULDER);
        strJudge += strMessage;
        for (int i = 0; i< m_listShoulder.Count; i++)
        {
            GetJudgementMessageString(ref strMessage, bDetail, m_listShoulder[i]);
            strJudge += strMessage;
            strJudge += "\r\n";
        }
        strJudge += "\r\n";
    }

    if (!(m_listNeck.Count == 0))
    {
        GetMessagePartTitleString(ref strMessage, pchTitle, Constants.JUDGEMENT_MESSAGEPARTID_NECK);
        strJudge += strMessage;
        for (int i = 0; i < m_listNeck.Count; i++)
        {
            GetJudgementMessageString(ref strMessage, bDetail, m_listNeck[i]);
            strJudge += strMessage;
            strJudge += "\r\n";
        }
        strJudge += "\r\n";
    }
}



    }
}
