﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Yugamiru
{
    public class MyImage
    {
        byte[] m_pSnapImage_standing;   // Œ¹‰æ‘œ‚Ì‰æ‘œ(DIB)ƒf[ƒ^ —§ˆÊ
        byte[] m_pSnapImage_kneedown;   // ÀˆÊ
        byte[] m_pSnapImage_side;   // ‘¤–Ê
        int m_iSize;    // ƒJƒƒ‰ BufferSize
        int m_iWidth;   // ƒJƒƒ‰ Width
        int m_iHeight;  // ƒJƒƒ‰ Height

        struct _IplImage
        {
            };

     public  MyImage()
{
	m_pSnapImage_standing	= null;
	m_pSnapImage_kneedown	= null;
	m_pSnapImage_side		= null;

	m_iSize = 0;
	m_iHeight = 0;
	m_iWidth = 0;	
}

    ~MyImage()
    {
        if (m_pSnapImage_standing != null)
        {
            //delete[] m_pSnapImage_standing;
            m_pSnapImage_standing = null;
        }
        if (m_pSnapImage_kneedown != null)
        {
            //delete[] m_pSnapImage_kneedown;
            m_pSnapImage_kneedown = null;
        }
        if (m_pSnapImage_side != null)
        {
            //delete[] m_pSnapImage_side;
            m_pSnapImage_side = null;
        }
    }

    public int GetWidth() 
{
	return m_iWidth;
}

        public int GetHeight() 
{
	return m_iHeight;
}

        public int GetBitCount() 
{
	return 24;
}

        public bool CreateBmpInfo(int iSize, int iWidth, int iHeight)
{
    m_iSize = iSize;
    m_iWidth = iWidth;
    m_iHeight = iHeight;
    return true;
}

        public bool ClearStandingImage()
{
    if (m_pSnapImage_standing != null)
    {
        //delete[] m_pSnapImage_standing;
        m_pSnapImage_standing = null;
    }
    return true;
}

public bool ClearKneedownImage()
{
    if (m_pSnapImage_kneedown != null)
    {
        //delete[] m_pSnapImage_kneedown;
        m_pSnapImage_kneedown = null;
    }
    return true;
}

        public bool ClearSideImage()
{
    if (m_pSnapImage_side != null)
    {
        //delete[] m_pSnapImage_side;
        m_pSnapImage_side = null;
    }
    return true;
}

        public void AllocStandingImage( )
{
    if (m_pSnapImage_standing == null)
    {
        m_pSnapImage_standing = new byte[1280 * 1024 * 3];
    }
    int i = 0;
    for (i = 0; i < 1280 * 1024; i++)
    {
        m_pSnapImage_standing[3 * i + 0] = 0;
        m_pSnapImage_standing[3 * i + 1] = 0;
        m_pSnapImage_standing[3 * i + 2] = 0;
    }
}

public void AllocKneedownImage()
{
    if (m_pSnapImage_kneedown == null)
    {
        m_pSnapImage_kneedown = new byte[1280 * 1024 * 3];
    }
    int i = 0;
    for (i = 0; i < 1280 * 1024; i++)
    {
        m_pSnapImage_kneedown[3 * i + 0] = 0;
        m_pSnapImage_kneedown[3 * i + 1] = 0;
        m_pSnapImage_kneedown[3 * i + 2] = 0;
    }
}

        public void AllocSideImage()
{
    if (m_pSnapImage_side == null)
    {
        m_pSnapImage_side = new byte[1280 * 1024 * 3];
    }
    int i = 0;
    for (i = 0; i < 1280 * 1024; i++)
    {
        m_pSnapImage_side[3 * i + 0] = 0;
        m_pSnapImage_side[3 * i + 1] = 0;
        m_pSnapImage_side[3 * i + 2] = 0;
    }
}

public void DrawStandingImage(Graphics hDC, Rectangle rc, int mag /*=1*/ )
{
    DrawImage(m_pSnapImage_standing, hDC, rc, mag);
}

void DrawKneedownImage(Graphics hDC, Rectangle rc, int mag /*=1*/ )
{
    DrawImage(m_pSnapImage_kneedown, hDC, rc, mag);
}

void DrawSideImage(Graphics hDC, Rectangle rc, int mag /*=1*/ )
{
    DrawImage(m_pSnapImage_side, hDC, rc, mag);
}

//‰æ‘œ‚Ì•\Ž¦iŠg‘åj
void DrawImage(byte[] pImage, Graphics hDC, Rectangle rc, int mag)
{
    int width = (rc.Right - rc.Left) * mag;
    int height = (rc.Bottom - rc.Top) * mag;

            /* Bitmap BMPInfo;
             BMPInfo.Size = sizeof(BITMAPINFOHEADER);
             BMPInfo.bmiHeader.biWidth = m_iWidth;
             BMPInfo.bmiHeader.biHeight = m_iHeight;
             BMPInfo.bmiHeader.biPlanes = 1;
             BMPInfo.bmiHeader.biBitCount = 24;
             BMPInfo.bmiHeader.biCompression = 0;
             BMPInfo.bmiHeader.biSizeImage = 0;
             BMPInfo.bmiHeader.biXPelsPerMeter = 0;
             BMPInfo.bmiHeader.biYPelsPerMeter = 0;
             BMPInfo.bmiHeader.biClrUsed = 0;
             BMPInfo.bmiHeader.biClrImportant = 0;

             BMPInfo.bmiColors[0].rgbBlue = 255;
             BMPInfo.bmiColors[0].rgbGreen = 255;
             BMPInfo.bmiColors[0].rgbRed = 255;
             BMPInfo.bmiColors[0].rgbReserved = 255;

             StretchDIBits(hDC,
                 0,
                 0,
                 width,
                 height,
                 0,
                 m_iHeight - 1 - (0 + m_iHeight - 1),
                 m_iWidth,
                 m_iHeight,
                 pImage, &BMPInfo, DIB_RGB_COLORS, SRCCOPY);*/
            var output = new Bitmap(m_iWidth, m_iHeight);
            var rect = new Rectangle(0, 0, width, height);
            var bmpData = output.LockBits(rect,
                ImageLockMode.ReadWrite, output.PixelFormat);
            var ptr = bmpData.Scan0;
            Marshal.Copy(pImage, 0, ptr, pImage.Length);
            output.UnlockBits(bmpData);
            Bitmap BMPInfo = output;
        }

public byte[] GetStandingImagePointer() 
{
	return m_pSnapImage_standing;
}

public byte[] GetKneedownImagePointer() 
{
	return m_pSnapImage_kneedown;
}

        public byte[] GetSideImagePointer() 
{
	return m_pSnapImage_side;
}

 /* 
public int CalcDataSizeOfImageAssignmentStatementAfterConversionToJPEG( byte[] pchSymbolName, byte[] pbyteImage, int iWidth, int iHeight)
{
            
#if 1
	unsigned char *pbyteEncodeBuffer = new unsigned char [ iWidth * iHeight * 3 ];
	unsigned char *imageData = new unsigned char [ iWidth * iHeight * 3 ];
	int iBmpWidthStep = ( iWidth * 3 + 3 ) / 4 * 4;
	{
		int i = 0;
		for( i = 0; i < iHeight; i++ ){
			int iBmpBaseIndex = (iHeight - 1 - i) * iBmpWidthStep;
			int iIplBaseIndex = i * iWidth * 3;
			int j = 0;
			for( j = 0; j < iWidth; j ++ ){
				imageData[iIplBaseIndex++] = pbyteImage[iBmpBaseIndex++];
				imageData[iIplBaseIndex++] = pbyteImage[iBmpBaseIndex++];
				imageData[iIplBaseIndex++] = pbyteImage[iBmpBaseIndex++];
			}
		}
	}
	int iEncodedImageSize = jpeg_encode( imageData, iWidth, iHeight, pbyteEncodeBuffer, 55 );
	if ( imageData != NULL ){
		delete [] imageData;
		imageData = NULL;
	}
	if ( pbyteEncodeBuffer != NULL ){
		delete [] pbyteEncodeBuffer;
		pbyteEncodeBuffer = NULL;
	}
#else
    IplImage* iplTmp = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 3);
    if (iplTmp == NULL)
    {
        return 0;
    }
    int iBmpWidthStep = (iWidth * 3 + 3) / 4 * 4;
    {
        int i = 0;
        for (i = 0; i < iHeight; i++)
        {
            int iBmpBaseIndex = (iHeight - 1 - i) * iBmpWidthStep;
            int iIplBaseIndex = i * iplTmp->widthStep;
            int j = 0;
            for (j = 0; j < iWidth; j++)
            {
                iplTmp->imageData[iIplBaseIndex++] = pbyteImage[iBmpBaseIndex++];
                iplTmp->imageData[iIplBaseIndex++] = pbyteImage[iBmpBaseIndex++];
                iplTmp->imageData[iIplBaseIndex++] = pbyteImage[iBmpBaseIndex++];
            }
        }
    }
    unsigned char* pbyteEncodeBuffer = new unsigned char[iplTmp->width * iplTmp->height * iplTmp->nChannels];
    if (pbyteEncodeBuffer == NULL)
    {
        if (iplTmp != NULL)
        {
            cvReleaseImage(&iplTmp);
            iplTmp = NULL;
        }
        return 0;
    }
    int iEncodedImageSize = cvexJPEGEncode(iplTmp, pbyteEncodeBuffer, 55);

    if (iplTmp != NULL)
    {
        cvReleaseImage(&iplTmp);
        iplTmp = NULL;
    }
    if (pbyteEncodeBuffer != NULL)
    {
        delete[] pbyteEncodeBuffer;
        pbyteEncodeBuffer = NULL;
    }
#endif
    int iRet = CalcDataSizeOfImageAssignmentStatement(pchSymbolName, iEncodedImageSize);
    return iRet;
}

int CMyImage::CalcDataSizeOfStatementBlock(void ) const
{
	int iRet = 0;
iRet += CalcDataSizeOfImageAssignmentStatementAfterConversionToJPEG( "StandingImage", m_pSnapImage_standing, m_iWidth, m_iHeight ) + sizeof(int);
	iRet += CalcDataSizeOfImageAssignmentStatementAfterConversionToJPEG( "KneedownImage", m_pSnapImage_kneedown, m_iWidth, m_iHeight ) + sizeof(int);
	iRet += CalcDataSizeOfImageAssignmentStatementAfterConversionToJPEG( "SideImage", m_pSnapImage_side, m_iWidth, m_iHeight ) + sizeof(int);
	return iRet;
}

int CMyImage::WriteImageAssignmentStatementAfterConversionToJPEG(unsigned char* pbyteOut, int iOffset, int iSize, const char* pchSymbolName, const unsigned char* pbyteImage, int iWidth, int iHeight)
{
    IplImage* iplTmp = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 3);
    if (iplTmp == NULL)
    {
        return 0;
    }
    int iBmpWidthStep = (iWidth * 3 + 3) / 4 * 4;
    {
        int i = 0;
        for (i = 0; i < iHeight; i++)
        {
            int iBmpBaseIndex = (iHeight - 1 - i) * iBmpWidthStep;
            int iIplBaseIndex = i * iplTmp->widthStep;
            int j = 0;
            for (j = 0; j < iWidth; j++)
            {
                iplTmp->imageData[iIplBaseIndex++] = pbyteImage[iBmpBaseIndex++];
                iplTmp->imageData[iIplBaseIndex++] = pbyteImage[iBmpBaseIndex++];
                iplTmp->imageData[iIplBaseIndex++] = pbyteImage[iBmpBaseIndex++];
            }
        }
    }
    unsigned char* pbyteEncodeBuffer = new unsigned char[iplTmp->width * iplTmp->height * iplTmp->nChannels];
    if (pbyteEncodeBuffer == NULL)
    {
        if (iplTmp != NULL)
        {
            cvReleaseImage(&iplTmp);
            iplTmp = NULL;
        }
        return 0;
    }
    int iEncodedImageSize = cvexJPEGEncode(iplTmp, pbyteEncodeBuffer, 55);
    iOffset = WriteImageAssignmentStatement(pbyteOut, iOffset, iSize, pchSymbolName, (unsigned char *)pbyteEncodeBuffer, iEncodedImageSize );

    if (iplTmp != NULL)
    {
        cvReleaseImage(&iplTmp);
        iplTmp = NULL;
    }
    if (pbyteEncodeBuffer != NULL)
    {
        delete[] pbyteEncodeBuffer;
        pbyteEncodeBuffer = NULL;
    }
    return iOffset;
}

int CMyImage::WriteStatementBlock(unsigned char* pbyteOut, int iOffset, int iSize) const
{
	iOffset = WriteImageAssignmentStatementAfterConversionToJPEG(pbyteOut, iOffset, iSize, "StandingImage", m_pSnapImage_standing, m_iWidth, m_iHeight );
iOffset = WriteImageAssignmentStatementAfterConversionToJPEG(pbyteOut, iOffset, iSize, "KneedownImage", m_pSnapImage_kneedown, m_iWidth, m_iHeight );
iOffset = WriteImageAssignmentStatementAfterConversionToJPEG(pbyteOut, iOffset, iSize, "SideImage", m_pSnapImage_side, m_iWidth, m_iHeight );
	return iOffset;
}


IplImage* CMyImage::DecodeJPEGImage( const unsigned char* pbyteSrc, int iSrcImageSize)
{
    unsigned char* pbyteTmpBuffer = new unsigned char[iSrcImageSize];
    if (pbyteTmpBuffer == NULL)
    {
        return NULL;
    }
    memcpy(pbyteTmpBuffer, pbyteSrc, iSrcImageSize);
    IplImage* piplImage = cvexJPEGDecode(pbyteTmpBuffer, iSrcImageSize, false);
    if (pbyteTmpBuffer != NULL)
    {
        delete[] pbyteTmpBuffer;
        pbyteTmpBuffer = NULL;
    }
    return piplImage;
}

int CMyImage::SetStandingIplImage( const IplImage* piplSrc)
{
    if (m_pSnapImage_standing != NULL)
    {
        delete[] m_pSnapImage_standing;
        m_pSnapImage_standing = NULL;
    }
    int iBmpWidthStep = (piplSrc->width * 3 + 3) / 4 * 4;
    m_pSnapImage_standing = new unsigned char[iBmpWidthStep * piplSrc->height];
    if (m_pSnapImage_standing == NULL)
    {
        return 0;
    }
    int i = 0;
    for (i = 0; i < piplSrc->height; i++)
    {
        int iBmpBaseIndex = (piplSrc->height - 1 - i) * iBmpWidthStep;
        int iIplBaseIndex = i * piplSrc->widthStep;
        int j = 0;
        for (j = 0; j < piplSrc->width; j++)
        {
            m_pSnapImage_standing[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
            m_pSnapImage_standing[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
            m_pSnapImage_standing[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
        }
    }
    return 1;
}

int CMyImage::SetKneedownIplImage( const IplImage* piplSrc)
{
    if (m_pSnapImage_kneedown != NULL)
    {
        delete[] m_pSnapImage_kneedown;
        m_pSnapImage_kneedown = NULL;
    }
    int iBmpWidthStep = (piplSrc->width * 3 + 3) / 4 * 4;
    m_pSnapImage_kneedown = new unsigned char[iBmpWidthStep * piplSrc->height];
    if (m_pSnapImage_kneedown == NULL)
    {
        return 0;
    }
    int i = 0;
    for (i = 0; i < piplSrc->height; i++)
    {
        int iBmpBaseIndex = (piplSrc->height - 1 - i) * iBmpWidthStep;
        int iIplBaseIndex = i * piplSrc->widthStep;
        int j = 0;
        for (j = 0; j < piplSrc->width; j++)
        {
            m_pSnapImage_kneedown[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
            m_pSnapImage_kneedown[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
            m_pSnapImage_kneedown[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
        }
    }
    return 1;
}

int CMyImage::SetSideIplImage( const IplImage* piplSrc)
{
    if (m_pSnapImage_side != NULL)
    {
        delete[] m_pSnapImage_side;
        m_pSnapImage_side = NULL;
    }
    int iBmpWidthStep = (piplSrc->width * 3 + 3) / 4 * 4;
    m_pSnapImage_side = new unsigned char[iBmpWidthStep * piplSrc->height];
    if (m_pSnapImage_side == NULL)
    {
        return 0;
    }
    int i = 0;
    for (i = 0; i < piplSrc->height; i++)
    {
        int iBmpBaseIndex = (piplSrc->height - 1 - i) * iBmpWidthStep;
        int iIplBaseIndex = i * piplSrc->widthStep;
        int j = 0;
        for (j = 0; j < piplSrc->width; j++)
        {
            m_pSnapImage_side[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
            m_pSnapImage_side[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
            m_pSnapImage_side[iBmpBaseIndex++] = piplSrc->imageData[iIplBaseIndex++];
        }
    }
    return 1;
}

int CMyImage::SetStandingJPEGImage( const unsigned char* pbyteSrc, int iSrcImageSize)
{
    IplImage* iplTmp = DecodeJPEGImage(pbyteSrc, iSrcImageSize);
    if (iplTmp == NULL)
    {
        return 0;
    }
    SetStandingIplImage(iplTmp);
    cvReleaseImage(&iplTmp);
    iplTmp = NULL;
    return 1;
}

int CMyImage::SetKneedownJPEGImage( const unsigned char* pbyteSrc, int iSrcImageSize)
{
    IplImage* iplTmp = DecodeJPEGImage(pbyteSrc, iSrcImageSize);
    if (iplTmp == NULL)
    {
        return 0;
    }
    SetKneedownIplImage(iplTmp);
    cvReleaseImage(&iplTmp);
    iplTmp = NULL;
    return 1;
}

int CMyImage::SetSideJPEGImage( const unsigned char* pbyteSrc, int iSrcImageSize)
{
    IplImage* iplTmp = DecodeJPEGImage(pbyteSrc, iSrcImageSize);
    if (iplTmp == NULL)
    {
        return 0;
    }
    SetSideIplImage(iplTmp);
    cvReleaseImage(&iplTmp);
    iplTmp = NULL;
    return 1;
}

int CMyImage::ExecuteImageAssignmentStatement( const char* pchSymbolName, int iSymbolNameLength, const unsigned char* pbyteImage, int iImageSize)
{
    if (CompareSymbol(pchSymbolName, iSymbolNameLength, "StandingImage"))
    {
        SetStandingJPEGImage(pbyteImage, iImageSize);
        return 1;
    }
    if (CompareSymbol(pchSymbolName, iSymbolNameLength, "KneedownImage"))
    {
        SetKneedownJPEGImage(pbyteImage, iImageSize);
        return 1;
    }
    if (CompareSymbol(pchSymbolName, iSymbolNameLength, "SideImage"))
    {
        SetSideJPEGImage(pbyteImage, iImageSize);
        return 1;
    }
    return 0;
}

int CMyImage::ExecuteIntegerAssignmentStatement( const char* pchSymbolName, int iSymbolNameLength, int iValue)
{
    return 0;
}

int CMyImage::ExecuteStringAssignmentStatement( const char* pchSymbolName, int iSymbolNameLength, const char* pchValue, int iValueLength)
{
    return 0;
}
*/
    }
}
